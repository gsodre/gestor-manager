const setupCard = theme => ({
  card: {
    marginBottom: 20,
    overflow: "visible"
  },

  header: {
    backgroundColor: "#f5f5f5",
    paddingBottom: 5,
    paddingTop: 10,
    paddingLeft: 15,
    paddingRight: 15
  },

  margin: {
    margin: theme.spacing.unit
  },

  extendedIcon: {
    marginRight: theme.spacing.unit
  },
  loader: {
    zIndex: 99999,
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -20,
    marginLeft: -20
  }
});

export default setupCard;
