import React from "react";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import Grid from "@material-ui/core/Grid";

const Select = ({ states, grid, options, ...rest }) => (
  <Grid item xs={grid.xs} md={grid.md}>
    <TextField
      // disabled={states.isLoading}
      select
      fullWidth
      variant="outlined"
      // label={label}
      // value={value}
      // onChange={onChange}
      margin="dense"
      InputLabelProps={{
        shrink: true
      }}
      {...rest}
    >
      {options.map(item => (
        <MenuItem value={item.value} key={item.value}>
          {item.text}
        </MenuItem>
      ))}
    </TextField>
  </Grid>
);

export default Select;
