import React from "react";
import MUIDataTable from "mui-datatables";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";

const translate = {
  textLabels: {
    body: {
      noMatch: "Nenhum resultado encontrado.",
      toolTip: "Ordenar"
    },
    pagination: {
      next: "Avançar",
      previous: "Voltar",
      rowsPerPage: "Linhas por página:",
      displayRows: "de"
    },
    toolbar: {
      search: "Pesquisar",
      downloadCsv: "Download CSV",
      print: "Imprimir",
      viewColumns: "Vizualizar Colunas",
      filterTable: "Filtros"
    },
    filter: {
      all: "Todos",
      title: "FILTROS",
      reset: "LIMPAR"
    },
    viewColumns: {
      title: "Mostrar Colunas",
      titleAria: "Mostrar/Ocultara Colunas"
    },
    selectedRows: {
      text: "Linha(s) seleciona(s)",
      delete: "Apagar",
      deleteAria: "Apagar Linha Selecionada"
    }
  }
};

const getMuiTheme = () =>
  createMuiTheme({
    typography: {
      useNextVariants: true
    },
    overrides: {
      MUIDataTableBodyCell: {
        root: {
          cursor: "pointer"
        }
      },
      MUIDataTable: {
        responsiveScroll: {
          maxHeight: "580px"
        }
      }
    }
  });

function Table({ ...props }) {
  const { states, options, ...rest } = props;
  return (
    <MuiThemeProvider theme={getMuiTheme}>
      <MUIDataTable {...rest} options={Object.assign(options, translate)} />
    </MuiThemeProvider>
  );
}

export default Table;
