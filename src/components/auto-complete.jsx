import React from "react";
import Select from "react-select";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import Grid from "@material-ui/core/Grid";

const styles = theme => ({
  input: {
    display: "flex",
    padding: 10
  },
  noOptionsMessage: {
    padding: `${theme.spacing.unit}px ${theme.spacing.unit * 2}px`
  }
});

function NoOptionsMessage(props) {
  return (
    <Typography
      color="textSecondary"
      className={props.selectProps.classes.noOptionsMessage}
      {...props.innerProps}
    >
      {props.children}
    </Typography>
  );
}

function inputComponent({ inputRef, ...props }) {
  return <div ref={inputRef} {...props} />;
}

function Control(props) {
  return (
    <TextField
      disabled
      fullWidth
      variant="outlined"
      InputProps={{
        inputComponent,
        inputProps: {
          className: props.selectProps.classes.input,
          inputRef: props.innerRef,
          children: props.children,
          ...props.innerProps
        }
      }}
      {...props.selectProps.textFieldProps}
    />
  );
}

function Option(props) {
  return (
    <MenuItem
      buttonRef={props.innerRef}
      selected={props.isFocused}
      component="div"
      style={{ fontWeight: props.isSelected ? 500 : 400 }}
      {...props.innerProps}
    >
      {props.children}
    </MenuItem>
  );
}

const components = { Control, NoOptionsMessage, Option };

function LiveSearch(props) {
  const { states, classes, grid, options, onChange, ...rest } = props;

  return (
    <Grid item xs={grid.xs} md={grid.md}>
      <Select
        {...rest}
        textFieldProps={{
          ...rest,
          InputLabelProps: {
            shrink: true
          }
        }}
        className="select-live-search"
        classes={classes}
        options={options}
        components={components}
        noOptionsMessage={() => "Nada encontrado.."}
        isClearable
        onChange={onChange}
      />
    </Grid>
  );
}

export default withStyles(styles)(LiveSearch);
