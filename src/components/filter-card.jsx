import React from "react";
import classNames from "classnames";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Icon from "@material-ui/core/Icon";

import Select from "~/components/select-simple";
import LiveSearch from "~/components/auto-complete";

const FilterCard = ({
  states,
  classes,
  title,
  btAction,
  options,
  handleChange,
  getData,
  handleClear
}) => (
  <Card className={classes.card}>
    <CardHeader className={classes.header} title={title} />
    <CardContent>
      <form>
        <Grid container justify="center" spacing={1}>
          <LiveSearch
            isDisabled={states.isLoading}
            placeholder="Selecione"
            states={states}
            label="Franquia"
            grid={{ xs: 6, md: 5 }}
            options={states.franquia}
            onChange={handleChange("idFranquia")}
          />
          <Select
            loading={states.isLoading}
            states={states.status}
            value={states.status}
            label="Status"
            options={options.status[0]}
            classes={classes}
            grid={{ xs: 6, md: 3 }}
            onChange={handleChange("status")}
          />
        </Grid>
      </form>
    </CardContent>
    <CardActions className={classes.actions} disableActionSpacing>
      <Grid justify="space-between" container>
        <Button
          disabled={states.isLoading}
          size="small"
          variant="contained"
          onClick={handleClear}
        >
          <Icon className={classNames(classes.icons, "fa fa-eraser")} />
          {"Limpar"}
        </Button>
        <Button
          disabled={states.isLoading}
          size="small"
          variant="contained"
          color="primary"
          onClick={getData}
        >
          <Icon className={classNames(classes.icons, btAction.ico)} />
          {btAction.text}
        </Button>
      </Grid>
    </CardActions>
  </Card>
);

export default FilterCard;
