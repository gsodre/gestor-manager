import React from "react";
import classNames from "classnames";
import withStyles from "@material-ui/core/styles/withStyles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Icon from "@material-ui/core/Icon";

import filterCard from "~/assets/jss/components/filter-card-style";

function FilterCard({ ...props }) {
  const {
    states,
    classes,
    title,
    btClear,
    btAction,
    children,
    handleClear,
    getData
  } = props;

  return (
    <Card className={classes.card}>
      <CardHeader className={classes.header} title={title} />
      <CardContent>{children}</CardContent>
      <CardActions className={classes.actions} disableActionSpacing>
        <Grid justify={btClear ? "space-between" : "flex-end"} container>
          {btClear && (
            <Button
              disabled={states.isLoading}
              size="small"
              variant="contained"
              onClick={handleClear}
            >
              <Icon className={classNames(classes.icons, "fa fa-eraser")} />
              {"Limpar"}
            </Button>
          )}
          <Button
            disabled={states.isLoading}
            size="small"
            variant="contained"
            color="primary"
            onClick={getData}
          >
            <Icon className={classNames(classes.icons, btAction.ico)} />
            {btAction.text}
          </Button>
        </Grid>
      </CardActions>
    </Card>
  );
}

export default withStyles(filterCard)(FilterCard);
