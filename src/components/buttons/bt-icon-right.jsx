import React from "react";
import classNames from "classnames";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Icon from "@material-ui/core/Icon";

const ButtonRight = ({ classes, grid, label, ico, icoSide, ...rest }) => (
  <Grid
    item
    className="bt-baseline"
    xs={grid.xs}
    sm={grid.sm}
    md={grid.md}
    lg={grid.lg}
    xl={grid.xl}
  >
    <Button {...rest} size="small" variant="contained">
      {icoSide === "left" && <Icon className={classNames("bt-icons", ico)} />}
      {label}
      {icoSide === "right" && <Icon className={classNames("bt-icons", ico)} />}
    </Button>
  </Grid>
);

export default ButtonRight;
