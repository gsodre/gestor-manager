import React from "react";
import classNames from "classnames";
import Drawer from "@material-ui/core/Drawer";
import IconButton from "@material-ui/core/IconButton";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import Divider from "@material-ui/core/Divider";

import MenuList from "./menu-list";
import logo from "~/images/logogray.png";

const MenuDrawer = ({
  theme,
  states,
  classes,
  handleDrawerClose,
  toggleMenu
}) => (
  <Drawer
    variant="permanent"
    className={classNames(classes.drawer, {
      [classes.drawerOpen]: states.drawerOpen,
      [classes.drawerClose]: !states.drawerOpen
    })}
    classes={{
      paper: classNames({
        [classes.drawerOpen]: states.drawerOpen,
        [classes.drawerClose]: !states.drawerOpen
      })
    }}
    open={states.drawerOpen}
  >
    <div className={classes.toolbar}>
      {states.drawerOpen && (
        <img className={classes.logoGray} src={logo} alt="Cigam" />
      )}
      <IconButton onClick={handleDrawerClose}>
        {theme.direction === "rtl" ? <ChevronRightIcon /> : <ChevronLeftIcon />}
      </IconButton>
    </div>
    <Divider />
    <MenuList classes={classes} states={states} toggleMenu={toggleMenu} />
  </Drawer>
);

export default MenuDrawer;
