import React from "react";
import Icon from "@material-ui/core/Icon";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Tooltip from "@material-ui/core/Tooltip";
import { Link } from "react-router-dom";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import Collapse from "@material-ui/core/Collapse";

const menu = [
  {
    text: "NPS",
    icon: "fas fa-tachometer-alt",
    path: "nps",
    hasSon: true,
    sub: [
      {
        text: "Dashboard",
        path: "/nps/dashboard"
      },
      {
        text: "Relatório NPS",
        path: "/nps/report"
      },
      {
        text: "Relatório Persistidos",
        path: "/nps/rel-persistidos"
      },
      {
        text: "Configurações",
        path: "/nps/config"
      }
    ]
  },
  {
    text: "PortalWeb",
    icon: "fas fa-globe-americas",
    path: "web",
    hasSon: true,
    sub: [
      {
        text: "Atualizador API",
        path: "/web/setup"
      },
      {
        text: "Monitor de Retaguarda",
        path: "/web/monitor"
      }
    ]
  },
  {
    text: "PAF ECF",
    icon: "fas fa-cash-register",
    path: "pafecf",
    hasSon: true,
    sub: [
      {
        text: "Bloco X",
        path: "/paf/blocox"
      }
    ]
  },
  {
    text: "Cartões",
    icon: "fas fa-credit-card",
    path: "cards",
    hasSon: true,
    sub: [
      {
        text: "Relatório Conciliação",
        path: "/cartao/conciliacao"
      }
    ]
  },
  {
    text: "Alertas",
    icon: "fas fa-exclamation-triangle",
    path: "alertas",
    hasSon: true,
    sub: [
      {
        text: "Relatório",
        path: "/alertas/relatorio"
      },
      {
        text: "Cadastro",
        path: "/alertas/cadastro"
      }
    ]
  }
];

const MenuList = ({ classes, states, toggleMenu }) => (
  <List>
    {menu.map(item => (
      <Tooltip
        title={item.text}
        placement="right"
        enterDelay={300}
        key={item.text}
      >
        {item.hasSon ? (
          <section>
            <ListItem button onClick={toggleMenu(item.path)}>
              <ListItemIcon>
                <Icon className={item.icon} style={{ width: "1.2em" }} />
              </ListItemIcon>
              <ListItemText className={classes.menuText} primary={item.text} />
              {states[item.path] ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={states[item.path]} timeout="auto" unmountOnExit>
              {item.sub.map(item => (
                <List component="div" disablePadding key={item.text}>
                  <ListItem button className={classes.nested}>
                    <Link to={item.path} className={classes.link}>
                      <ListItemText inset primary={item.text} />
                    </Link>
                  </ListItem>
                </List>
              ))}
            </Collapse>
          </section>
        ) : (
          <Link to={item.path} className={classes.link}>
            <ListItem button key={item.text}>
              <ListItemIcon>
                <Icon className={item.icon} style={{ width: "1.2em" }} />
              </ListItemIcon>
              <ListItemText className={classes.menuText} primary={item.text} />
            </ListItem>
          </Link>
        )}
      </Tooltip>
    ))}
  </List>
);

export default MenuList;
