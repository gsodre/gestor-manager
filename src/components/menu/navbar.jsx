import React from "react";
import AppBar from "@material-ui/core/AppBar";
import classNames from "classnames";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import { Link } from "react-router-dom";
import Icon from "@material-ui/core/Icon";
import Tooltip from "@material-ui/core/Tooltip";

import logo from "~/images/logo.png";

const Navbar = ({ states, classes, handleDrawerOpen, handleOpenModal }) => (
  <AppBar
    position="fixed"
    className={classNames(classes.appBar, {
      [classes.appBarShift]: states.drawerOpen
    })}
  >
    <Toolbar disableGutters={!states.drawerOpen}>
      <IconButton
        color="inherit"
        aria-label="Open drawer"
        onClick={handleDrawerOpen}
        className={classNames(classes.menuButton, {
          [classes.hide]: states.drawerOpen
        })}
      >
        <MenuIcon />
      </IconButton>
      <Link to="/index">
        {!states.drawerOpen && (
          <img className={classes.logoWhite} src={logo} alt="Cigam" />
        )}
      </Link>
      <div className={classes.grow} />
      <section className={classes.btOut}>
        <Tooltip title="Sair" placement="left" enterDelay={300}>
          <IconButton
            aria-haspopup="true"
            color="inherit"
            onClick={handleOpenModal}
          >
            <Icon
              className={classNames(classes.icons, "fas fa-sign-out-alt")}
              fontSize="small"
            />
          </IconButton>
        </Tooltip>
      </section>
    </Toolbar>
  </AppBar>
);

export default Navbar;
