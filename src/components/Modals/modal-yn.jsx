import React from "react";
// import PropTypes from 'prop-types';
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Slide from "@material-ui/core/Slide";

function Transition(props) {
  return <Slide direction="down" {...props} />;
}

const ModalYesNo = ({ states, bts, handleNo, handleYes }) => (
  <Dialog
    open={states.open}
    TransitionComponent={Transition}
    keepMounted
    onClose={handleNo}
    aria-labelledby="alert-dialog-slide-title"
    aria-describedby="alert-dialog-slide-description"
  >
    <DialogTitle id="alert-dialog-slide-title">{states.title}</DialogTitle>
    <DialogContent>
      <DialogContentText id="alert-dialog-slide-description">
        {states.msg}
      </DialogContentText>
    </DialogContent>
    <DialogActions>
      <Button onClick={handleNo} variant="contained">
        {bts[0]}
      </Button>
      <Button onClick={handleYes} variant="contained">
        {bts[1]}
      </Button>
    </DialogActions>
  </Dialog>
);

// ModalYesNo.prototypes = {
//   states: PropTypes.shape({
//     open: PropTypes.bool.isRequired,
//     title: PropTypes.string.isRequired,
//     msg: PropTypes.string.isRequired,
//   }),
// };

export default ModalYesNo;
