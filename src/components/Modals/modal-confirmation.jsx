import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";

import withStyles from "@material-ui/core/styles/withStyles";

const style = {
  paper: {
    overflow: "visible"
  }
};

function ModalConfimation({ ...props }) {
  const {
    children,
    states,
    bts,
    handleCancel,
    handleConfirm,
    visible,
    overflow,
    ...rest
  } = props;

  return (
    <Dialog
      {...rest}
      fullWidth
      disableBackdropClick
      disableEscapeKeyDown
      open={states.modal.open}
      keepMounted
      // scroll="body"
    >
      <DialogTitle id="alert-dialog-slide-title">
        {states.modal.title}
      </DialogTitle>
      <DialogContent className={overflow}>{children}</DialogContent>
      <DialogActions>
        <Button
          disabled={states.isLoading}
          onClick={handleCancel}
          variant="contained"
        >
          {bts[0]}
        </Button>
        {visible && (
          <Button
            disabled={states.isLoading}
            variant="contained"
            color="primary"
            onClick={handleConfirm}
          >
            {bts[1]}
          </Button>
        )}
      </DialogActions>
    </Dialog>
  );
}

export default withStyles(style)(ModalConfimation);
