import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Grid from "@material-ui/core/Grid";

import Select from "./select-simple";

const ModalConfimation = ({
  classes,
  states,
  bts,
  handleCancel,
  handleConfirm,
  options,
  handleChange
}) => (
  <Dialog
    fullWidth
    maxWidth="md"
    disableBackdropClick
    disableEscapeKeyDown
    open={states.modal.open}
    keepMounted
  >
    <DialogTitle id="alert-dialog-slide-title">
      {"Loja: "}
      {states.modal.title}
    </DialogTitle>
    <DialogContent>
      <form>
        <Grid container spacing={1}>
          <Select
            states={states}
            value={states.days}
            label="Dias para Bloqueio"
            options={options.days}
            classes={classes}
            grid={{ xs: 12, md: 4 }}
            onChange={handleChange("days")}
          />
          <Select
            states={states}
            value={states.score}
            label="Obriga Justificativa"
            options={options.score}
            classes={classes}
            grid={{ xs: 12, md: 4 }}
            onChange={handleChange("score")}
          />
          <Select
            states={states}
            value={states.modalStatus}
            label="Status"
            options={options.status[1]}
            classes={classes}
            grid={{ xs: 12, md: 4 }}
            onChange={handleChange("modalStatus")}
          />
        </Grid>
      </form>
    </DialogContent>
    <DialogActions>
      <Button disabled={states.isLoading} onClick={handleCancel}>
        {bts[0]}
      </Button>
      <Button
        disabled={states.isLoading}
        onClick={handleConfirm}
        color="primary"
      >
        {bts[1]}
      </Button>
    </DialogActions>
  </Dialog>
);

export default ModalConfimation;
