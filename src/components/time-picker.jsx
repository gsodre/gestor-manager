import React from "react";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core/styles";
import { TimePicker } from "@material-ui/pickers";

const styles = {
  inputProps: {
    padding: 6
  }
};

const TPicker = ({
  states,
  ampm,
  autoOk,
  views,
  value,
  grid,
  label,
  minDate,
  maxDate,
  onChange
}) => (
  <Grid item xs={grid.xs} md={grid.md}>
    <div className="picker">
      <TimePicker
        disabled={states.isLoading}
        fullWidth
        ampm={ampm}
        autoOk={autoOk}
        variant="outlined"
        // views={views}
        // openTo="year"
        label={label}
        value={value}
        margin="dense"
        // minDate={minDate}
        // maxDate={maxDate}
        onChange={onChange}
      />
    </div>
  </Grid>
);

export default withStyles(styles)(TPicker);
