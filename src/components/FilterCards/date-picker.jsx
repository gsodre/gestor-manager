import React from "react";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core/styles";
import { DatePicker } from "@material-ui/pickers";

const styles = {
  inputProps: {
    padding: 6
  }
};

const Picker = ({ states, grid, ...rest }) => (
  <Grid item xs={grid.xs} md={grid.md}>
    <div className="picker">
      <DatePicker
        {...rest}
        disabled={states.isLoading}
        fullWidth
        inputVariant="outlined"
        margin="dense"
      />
    </div>
  </Grid>
);

export default withStyles(styles)(Picker);
