import React from "react";
import Checkbox from "@material-ui/core/Checkbox";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import CheckBoxOutlineBlankIcon from "@material-ui/icons/CheckBoxOutlineBlank";
import CheckBoxIcon from "@material-ui/icons/CheckBox";
import Grid from "@material-ui/core/Grid";

const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
const checkedIcon = <CheckBoxIcon fontSize="small" />;

function CheckboxesTags(props) {
  const { label, placeholder, grid, options, ...rest } = props;
  return (
    <Grid item xs={grid.xs} sm={grid.sm} md={grid.md} lg={grid.lg} xl={grid.xl}>
      <Autocomplete
        {...rest}
        multiple
        noOptionsText={"Nada encontrado.."}
        options={options}
        // defaultValue={options === undefined ? "" : [options[0]]}
        disableCloseOnSelect
        getOptionLabel={option => option.value}
        renderOption={(option, { selected }) => (
          <React.Fragment>
            <Checkbox
              icon={icon}
              checkedIcon={checkedIcon}
              checked={selected}
            />
            {option.label}
          </React.Fragment>
        )}
        renderInput={params => (
          <TextField
            {...params}
            variant="outlined"
            label={label}
            placeholder={placeholder}
            margin="dense"
            fullWidth
            InputLabelProps={{ shrink: true }}
          />
        )}
      />
    </Grid>
  );
}

export default CheckboxesTags;
