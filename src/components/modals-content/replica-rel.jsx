import React from "react";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import MenuItem from "@material-ui/core/MenuItem";

const Content = ({ states, handleChangeContato, handleChangeReplica }) => (
  <Grid container spacing={1}>
    <Table size="small">
      <TableHead>
        <TableRow>
          <TableCell>Loja</TableCell>
          <TableCell>Avaliador</TableCell>
          <TableCell>Função</TableCell>
          <TableCell>Menção</TableCell>
          <TableCell>Contato</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        <TableRow>
          <TableCell>{states.mLoja}</TableCell>
          <TableCell>{states.mAvaliador}</TableCell>
          <TableCell>{states.mFuncao}</TableCell>
          <TableCell>{states.mNota}</TableCell>
          <TableCell>
            {states.mContato !== undefined && states.mContato.length > 0 ? (
              <TextField
                select
                fullWidth
                value={states.mContatoVal}
                onChange={handleChangeContato}
              >
                {states.mContato.map(item => (
                  <MenuItem value={item} key={item}>
                    {item}
                  </MenuItem>
                ))}
              </TextField>
            ) : (
              "Sem Número"
            )}
          </TableCell>
        </TableRow>
      </TableBody>
    </Table>

    <TextField
      disabled={false}
      label="Justificativa"
      multiline
      fullWidth
      rows="3"
      value={states.mJustificativa}
      margin="normal"
      variant="outlined"
      InputLabelProps={{
        shrink: true
      }}
    />

    <TextField
      disabled={false}
      label="Réplica"
      multiline
      fullWidth
      rows="3"
      value={states.mReplica}
      margin="normal"
      variant="outlined"
      onChange={handleChangeReplica}
    />
  </Grid>
);

export default Content;
