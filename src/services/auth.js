export const TOKEN_KEY = btoa("@manager-Token");
export const TIME_OUT = btoa("@manager-TimeOut");
export const USER_NAME = btoa("@manager-User");

export const isAuthenticated = () => localStorage.getItem(TOKEN_KEY) !== null;

export const getToken = () => localStorage.getItem(TOKEN_KEY);
export const getUser = () => localStorage.getItem(USER_NAME);

export const login = (token, time, user) => {
  localStorage.setItem(TOKEN_KEY, token);
  localStorage.setItem(TIME_OUT, time);
  localStorage.setItem(USER_NAME, btoa(user));
};
export const logout = () => {
  localStorage.removeItem(TOKEN_KEY);
};
