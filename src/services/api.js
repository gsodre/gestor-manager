import axios from "axios";
import { getToken } from "./auth";
// import configs from "../../public/config";

const api = axios.create({
  // baseURL: "http://localhost/Gestor.Api.Gestorzito/"
  baseURL: "https://api.gestorzito.cigamgestor.com.br/"
});

api.interceptors.request.use(async config => {
  const token = await getToken();
  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});

const apiGestorzito = {
  // ---------- START AUTENTICAÇÃO ----------
  auth: hash =>
    api.post("api/AutenticacaoAD", hash, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Basic ${hash}`
      }
    }),
  // ---------- END AUTENTICAÇÃO ----------

  // ---------- START DEFAULTS ----------
  getFranquias: () => api.get("api/franquia/listar"),

  getLojas: (
    idFranquia,
    mostrarInativas,
    mostrarRetaguardas,
    mostrarDesinstaladas
  ) =>
    api.get("api/franquia/listarlojas", {
      params: {
        idFranquia,
        mostrarInativas,
        mostrarRetaguardas,
        mostrarDesinstaladas
      }
    }),

  // ---------- END DEFAULTS ----------

  // ---------- START NPS ----------
  getConfig: (idFranquia, status) =>
    api.get("api/configuracaonps/search", { params: { idFranquia, status } }),

  putConfig: config => api.put("api/configuracaonps/edit", config),

  getReport: (
    idFranquia,
    idLoja,
    dtInicial,
    dtFim,
    nps,
    quesito,
    comReplica,
    funcaoFiltro
  ) =>
    api.get("api/Resposta/Get", {
      params: {
        idFranquia,
        idLoja,
        dtInicial,
        dtFim,
        classificacao: nps,
        codNpsPergunta: quesito,
        comResposta: comReplica,
        funcao: funcaoFiltro
      }
    }),

  postReply: replica => api.post("api/resposta/replica", replica),

  getDashNps: (idFranquia, data) =>
    api.get("api/dashboard/get", { params: { idFranquia, data } }),

  getRelPersistidos: (
    idFranquia,
    idLoja,
    numMesesDetrator,
    classificacao,
    codNpsPergunta
  ) =>
    api.get("api/Resposta/RelatorioPersistido", {
      params: {
        idFranquia,
        loja: idLoja,
        numMesesDetrator,
        classificacao,
        codNpsPergunta
      }
    }),
  // ---------- END NPS ----------

  // ---------- START SEUTP APIS ----------
  getSetupApi: idFranquia =>
    api.get("api/setup/listar", { params: { idFranquia } }),

  postSetupApi: setupApi => api.post("api/setup/instalar", setupApi),

  postSetupAllApi: setupAllApi =>
    api.post("api/setup/instalarTodos", setupAllApi),

  getAllVersions: idFranquia =>
    api.get("api/setup/versoes", { params: { idFranquia } }),
  // ---------- END SEUTP APIS ----------

  // ---------- START PAF ECF BLOCO X ----------
  getPendencias: () => api.get("api/PafEcf/ConsultarPendencias"),

  getPendenciasContribuinte: inscricaoEstadual =>
    api.get(`api/PafEcf/ConsultarPendenciasContribuinte/${inscricaoEstadual}`)
  // ---------- END PAF ECF BLOCO X ----------
};

// ---------- START API CONCILIAÇÃO DE CARTÕES ----------
const apiConciliacaoCartoes = axios.create({
  // baseURL: "http://192.168.100.73:8085/Gestor.Api.ConciliacaoCartoes/"
  // baseURL: "http://187.86.153.229/Gestor.Api.ConciliacaoCartoes/"
  baseURL: "https://neogrid.cigamgestor.com.br/Gestor.Api.ConciliacaoCartoes/"
});

apiConciliacaoCartoes.interceptors.request.use(async config => {
  const token = await getToken();
  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});

export const apiCartoes = {
  getRelCards: (cnpj, dataInicio, dataFim, analitico) =>
    apiConciliacaoCartoes.get("relatorio", {
      params: { cnpj, dataInicio, dataFim, analitico }
    })
};
// ---------- END API CONCILIAÇÃO DE CARTÕES ----------

// ---------- START API ALERTAS ----------
const apiAlerta = axios.create({
  baseURL: "https://api.alerta.cigamgestor.com.br/Gestor.Api.Alerta.v2/"
  // baseURL: "https://api.alerta.cigamgestor.com.br/Gestor.Api.Alerta.homol/"
  // baseURL: "http://187.86.153.229/Gestor.Api.Alerta.Homol/"
  // baseURL: "http://192.168.100.108/Gestor.Api.Alerta/"
  // baseURL: "http://neogrid.cigamgestor.com.br/Gestor.Api.Alerta.Homol.V2/"
});

apiAlerta.interceptors.request.use(async config => {
  const token = await getToken();
  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});

export const apiAlertas = {
  getAlertas: (
    codFranqueadora,
    loja,
    dscAlertaSistema,
    flgTipoAlerta,
    flgStatus,
    flgAtivo,
    datInicio,
    datFim,
    horInicio,
    horFim,
    flgFecharAlerta,
    numApresentacao,
    flgTipoApresentacao
  ) =>
    apiAlerta.get("url/listarAlertas", {
      params: {
        codFranqueadora,
        loja,
        dscAlertaSistema,
        flgTipoAlerta,
        flgStatus,
        flgAtivo,
        datInicio,
        datFim,
        horInicio,
        horFim,
        flgFecharAlerta,
        numApresentacao,
        flgTipoApresentacao
      }
    }),

  getAlertasLoja: (codAlertaSistema, loja) =>
    apiAlerta.get("url/listarAlertasLoja", {
      params: { codAlertaSistema, loja }
    }),

  postAlerta: model => apiAlerta.post("url/adicionaralertagestor", model)
};
// ---------- END API ALERTAS ----------

export default apiGestorzito;
