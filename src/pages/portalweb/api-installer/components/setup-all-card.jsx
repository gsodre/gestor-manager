import React from "react";
import Grid from "@material-ui/core/Grid";
import Fab from "@material-ui/core/Fab";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import Settings from "@material-ui/icons/Settings";

import Select from "~/components/select-simple";

const ApiAllCard = ({
  states,
  classes,
  handleChange,
  handleInstall,
  api,
  versao
}) => (
  <Grid container justify="center" alignItems="center" spacing={0}>
    <Grid md={6}>
      <Card className={classes.card}>
        <CardHeader className={classes.header} title="Todas API's" />
        <CardContent>
          <Grid container justify="center" alignItems="center" spacing={0}>
            <Grid container justify="flex-end" md={6}>
              <Select
                states={states}
                label="Versão"
                disabled={states.isLoading}
                value={versao}
                options={api.map((item, index) =>
                  index === 0
                    ? { value: "Selecione", text: "Selecione" }
                    : { value: item, text: item }
                )}
                classes={classes}
                grid={{ xs: 12, md: 12 }}
                onChange={handleChange}
                InputLabelProps={{ shrink: true }}
              />
            </Grid>

            <Grid container justify="center" md={3}>
              <Grid item>
                <Fab
                  disabled={states.isLoading}
                  variant="extended"
                  size="medium"
                  color="primary"
                  aria-label="Add"
                  className={classes.margin}
                  onClick={handleInstall}
                >
                  <Settings className={classes.extendCached} />
                  {"Instalar"}
                </Fab>
              </Grid>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </Grid>
  </Grid>
);

export default ApiAllCard;
