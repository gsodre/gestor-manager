import React from "react";
import Grid from "@material-ui/core/Grid";
import Fab from "@material-ui/core/Fab";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import Avatar from "@material-ui/core/Avatar";
import Cached from "@material-ui/icons/Cached";
import Update from "@material-ui/icons/Update";
import FaceIcon from "@material-ui/icons/Face";
import Build from "@material-ui/icons/Build";
import Settings from "@material-ui/icons/Settings";
import ListItemText from "@material-ui/core/ListItemText";
import ListItem from "@material-ui/core/ListItem";

import Select from "~/components/select-simple";

const ApiCard = ({
  states,
  classes,
  handleChange,
  handleInstall,
  api,
  versao
}) => (
  <Card className={classes.card}>
    <CardHeader
      className={classes.header}
      title={`${api.apiNome} - ${
        api.versao !== null ? api.versao : "Não Instalada"
      }`}
    />
    <CardContent>
      <Grid container alignItems="center" spacing={0}>
        <Grid container md={6}>
          <Grid item>
            <ListItem>
              <Avatar>
                <Update />
              </Avatar>
              <ListItemText primary="Atualizado" secondary={api.atualizada} />
            </ListItem>
          </Grid>
          <Grid item>
            <ListItem>
              <Avatar>
                <FaceIcon />
              </Avatar>
              <ListItemText primary="Operador" secondary={api.operador} />
            </ListItem>
          </Grid>
          <Grid item>
            <ListItem>
              <Avatar>
                <Build />
              </Avatar>
              <ListItemText
                primary="Versão Anterior"
                secondary={api.versaoAnterior}
              />
            </ListItem>
          </Grid>
        </Grid>

        <Grid container justify="flex-end" md={3}>
          <Select
            id={api.apiNome.split(".").slice(-1)[0]}
            states={states}
            label="Versão"
            disabled={states.isLoading}
            value={Reflect.get(states, versao)}
            options={api.existentes.map(item => ({
              value: item,
              text: item
            }))}
            classes={classes}
            grid={{ xs: 12, md: 8 }}
            onChange={handleChange}
            InputLabelProps={{
              shrink: true
            }}
          />
        </Grid>

        <Grid container justify="center" md={3}>
          <Grid item>
            <Fab
              disabled={states.isLoading}
              variant="extended"
              size="medium"
              color="primary"
              aria-label="Add"
              className={classes.margin}
              onClick={handleInstall}
            >
              {api.versao !== null ? (
                <Cached className={classes.extendCached} />
              ) : (
                <Settings className={classes.extendCached} />
              )}
              {api.versao !== null ? "Atualizar" : "Instalar"}
            </Fab>
          </Grid>
        </Grid>
      </Grid>
    </CardContent>
  </Card>
);

export default ApiCard;
