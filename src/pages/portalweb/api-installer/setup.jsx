import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import withStyles from "@material-ui/core/styles/withStyles";
import Snackbar from "@material-ui/core/Snackbar";
import CircularProgress from "@material-ui/core/CircularProgress";

import { getUser } from "~/services/auth";
import Noty from "~/components/noty-alert";
import setupCard from "~/assets/jss/setup-style";
import api from "~/services/api";
import FilterCard from "~/components/cards/filter-card";
import LiveSearch from "~/components/auto-complete";
import ApiCard from "~/pages/portalweb/api-installer/components/setup-card";
import ApiAllCard from "~/pages/portalweb/api-installer/components/setup-all-card";

class Setup extends Component {
  state = {
    allVersions: [],
    allApis: "Selecione",
    isLoading: true,
    result: false,
    franquia: [],
    idFranquia: null,
    apis: [],
    alert: false
  };

  componentDidMount() {
    this.getFranquias();
  }

  handleClear = () => {
    this.setState({
      result: false,
      idFranquia: 0
    });
  };

  getData = async () => {
    console.log(this.state);
    const { idFranquia } = this.state;
    console.log(idFranquia);

    if (idFranquia !== null) {
      this.setState({ isLoading: true });

      try {
        const response = await api.getSetupApi(idFranquia);
        const respVersions = await api.getAllVersions(idFranquia);

        const allVersions = respVersions.data.map(item => item.name);

        response.data.map(item =>
          this.setState({ [item.apiNome.split(".").slice(-1)[0]]: item.versao })
        );

        this.setState({
          allVersions,
          apis: response.data,
          isLoading: false,
          result: true
        });
      } catch (error) {
        let value =
          error.response === undefined
            ? "Serviço Temporariamente Indisponível."
            : error.response.data;
        this.setState({
          isLoading: false,
          alert: true,
          alertIcon: "error",
          alertMessage: value
        });
      }
    } else {
      this.setState({
        alert: true,
        alertIcon: "warning",
        alertMessage: "Selecione uma Franquia!"
      });
    }
  };

  getFranquias = async () => {
    try {
      const response = await api.getFranquias();
      console.log(response);
      const franquia = response.data.map(item => ({
        value: item.idfranquia,
        label: item.nome,
        ret: item.dscenderecorede
      }));
      this.setState({ franquia, isLoading: false });
    } catch (error) {
      let value =
        error.response === undefined
          ? "Serviço Temporariamente Indisponível."
          : error.response.data;
      this.setState({
        error: value,
        isLoading: false
      });
    }
  };

  handleChange = opt => event => {
    console.log(this.state);

    let value = null;
    event === null
      ? (value = null)
      : (value = opt === "idFranquia" ? event.value : event.target.value);
    this.setState({
      [opt]: value
    });
    if (opt === "idFranquia") {
      this.setState(prevState => ({
        apis: []
      }));
    }

    console.log(this.state);
  };

  handleInstall = apiName => async event => {
    this.setState({ isLoading: true });

    const user = atob(getUser());
    let name = apiName.split(".").slice(-1)[0];
    let apiVersion = document.getElementById(name).value;
    const { idFranquia } = this.state;

    const setupApi = { idFranquia, user, apiName, apiVersion };

    try {
      const response = await api.postSetupApi(setupApi);
      if (response.status === 200) {
        this.setState({
          alert: true,
          alertIcon: "success",
          alertMessage: `Processo realizado com Sucesso!`
        });
        this.getData();
      }
      this.setState({ isLoading: false });
    } catch (error) {
      let value =
        error.response === undefined
          ? "Serviço Temporariamente Indisponível."
          : error.response.data;
      this.setState({
        error: value,
        isLoading: false
      });
    }
  };

  handleInstallAll = () => async () => {
    const user = atob(getUser());
    let apiVersion = this.state.allApis;
    const { idFranquia } = this.state;

    const setupApi = { idFranquia, user, apiVersion };

    console.log(setupApi);
    if (apiVersion !== "Selecione") {
      this.setState({ isLoading: true });
      try {
        const response = await api.postSetupAllApi(setupApi);
        if (response.status === 200) {
          this.setState({
            alert: true,
            alertIcon: "success",
            alertMessage: `Processo realizado com Sucesso!`
          });
          this.getData();
        }
        this.setState({ isLoading: false });
      } catch (error) {
        let value =
          error.response === undefined
            ? "Serviço Temporariamente Indisponível."
            : error.response.data;
        this.setState({
          error: value,
          isLoading: false
        });
      }
    } else {
      this.setState({
        alert: true,
        alertIcon: "warning",
        alertMessage: `Selecione uma Versão para a Instalação!`
      });
    }
  };

  handleClose = reason => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ alert: false });
  };

  render() {
    const { classes } = this.props;
    const states = this.state;

    return (
      <div>
        {states.isLoading && <CircularProgress className={classes.loader} />}
        <FilterCard
          title="Atualizador"
          states={states}
          handleClear={() => this.handleClear()}
          getData={() => this.getData()}
          btClear={false}
          btAction={{ ico: "fa fa-search", text: "Buscar" }}
        >
          <Grid container justify="center" spacing={1}>
            <LiveSearch
              isDisabled={states.isLoading}
              placeholder="Selecione"
              label="Franquia"
              states={states}
              grid={{ xs: 12, md: 4 }}
              options={states.franquia}
              onChange={this.handleChange("idFranquia")}
            />
          </Grid>
        </FilterCard>

        {states.result && (
          <ApiAllCard
            states={states}
            classes={classes}
            api={states.allVersions}
            handleInstall={this.handleInstallAll()}
            handleChange={this.handleChange("allApis")}
            versao={states.allApis}
          />
        )}

        {states.result &&
          states.apis.map((item, index) => (
            <ApiCard
              key={index}
              states={states}
              classes={classes}
              api={item}
              handleInstall={this.handleInstall(item.apiNome)}
              handleChange={this.handleChange(
                item.apiNome.split(".").slice(-1)[0]
              )}
              versao={item.apiNome.split(".").slice(-1)[0]}
            />
          ))}

        <Snackbar
          anchorOrigin={{
            vertical: "top",
            horizontal: "right"
          }}
          open={states.alert}
          autoHideDuration={3000}
          onClose={this.handleClose}
        >
          <Noty
            onClose={this.handleClose}
            variant={states.alertIcon}
            message={states.alertMessage}
          />
        </Snackbar>
      </div>
    );
  }
}

export default withStyles(setupCard)(Setup);
