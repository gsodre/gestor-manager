import { createMuiTheme } from '@material-ui/core/styles';

export const styles = theme => ({
  link: {
    textDecoration: 'none',
  },
  body: {
    padding: 24,
  },
  roots: {
    width: 280,
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  nested: {
    paddingLeft: theme.spacing.unit * 4,
  },

  list: {
    width: 250,
  },
  root: {
    width: '100%',
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  img: {
    height: '40px',
  },
});

export const theme = createMuiTheme({
  palette: {
    primary: { main: '#33414e' },
    secondary: { main: '#33414e' },
  },
  typography: {
    useNextVariants: true,
  },
});
