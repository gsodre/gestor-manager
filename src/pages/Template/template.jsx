import React, { Component } from "react";
import { MuiThemeProvider, withStyles } from "@material-ui/core/styles";
import { loadCSS } from "fg-loadcss/src/loadCSS";
import CssBaseline from "@material-ui/core/CssBaseline";
import { mainTheme, styles } from "./styles/styles-ico";
import Navbar from "~/components/menu/navbar";
import MenuDrawer from "~/components/menu/menu-drawer";

import ModalYesNo from "~/components/modals/modal-yn";
import { logout, getToken } from "~/services/auth";

class Template extends Component {
  state = {
    drawerOpen: false,
    logout: {
      open: false,
      title: "Atenção!",
      msg: "Deseja sair do sistema?"
    }
  };

  componentDidMount() {
    loadCSS(
      "https://use.fontawesome.com/releases/v5.11.2/css/all.css",
      document.querySelector("#insertion-point-jss")
    );

    const tk = getToken();
    this.setState({ tk });
  }

  handleDrawerOpen = () => {
    this.setState({ drawerOpen: true });
    console.log(this.state);
  };

  handleDrawerClose = () => {
    this.setState({
      drawerOpen: false,
      nps: false,
      web: false,
      pafecf: false,
      cards: false
    });
  };

  toggleMenu = menu => () => {
    this.setState(state => ({
      drawerOpen: true,
      [menu]: !state[menu]
    }));
  };

  handleOpenModal = () => {
    this.setState(prevState => ({
      logout: {
        open: true,
        title: prevState.logout.title,
        msg: prevState.logout.msg
      }
    }));
  };

  handleYes = async e => {
    const prop = this.props;
    e.preventDefault();
    logout(() => prop.history.push("/"));
    this.setState({ logout: false });
  };

  handleNo = () => {
    this.setState(prevState => ({
      logout: {
        open: false,
        title: prevState.logout.title,
        msg: prevState.logout.msg
      }
    }));
  };

  render() {
    const { classes, theme, children } = this.props;
    const states = this.state;

    return (
      <MuiThemeProvider theme={mainTheme}>
        <ModalYesNo
          states={states.logout}
          bts={["Não", "Sim"]}
          handleNo={() => this.handleNo()}
          handleYes={e => this.handleYes(e)}
        />
        <div className={classes.root}>
          <CssBaseline />
          <Navbar
            classes={classes}
            states={states}
            handleDrawerOpen={() => this.handleDrawerOpen()}
            handleOpenModal={() => this.handleOpenModal()}
          />
          <MenuDrawer
            theme={theme}
            classes={classes}
            states={states}
            handleDrawerClose={() => this.handleDrawerClose()}
            toggleMenu={menu => this.toggleMenu(menu)}
          />
          <main className={classes.content}>
            <div className={classes.toolbar} />
            {children}
          </main>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default withStyles(styles, { withTheme: true })(Template);
