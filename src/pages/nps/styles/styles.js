export const styles = {
  card: {
    marginBottom: 20,
    overflow: 'visible',
  },
  icons: {
    fontSize: 15,
    marginRight: 7,
  },
  header: {
    backgroundColor: '#f5f5f5',
    paddingBottom: 5,
    paddingTop: 10,
    paddingLeft: 15,
    paddingRight: 15,
  },
  actions: {
    backgroundColor: '#f5f5f5',
    display: 'flex',
  },
  loader: {
    zIndex: 99999,
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -20,
    marginLeft: -20,
  },
};
