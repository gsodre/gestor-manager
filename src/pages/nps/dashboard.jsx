import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Chart from "react-google-charts";
import Typography from "@material-ui/core/Typography";
import CircularProgress from "@material-ui/core/CircularProgress";
import Fab from "@material-ui/core/Fab";
import MoreVert from "@material-ui/icons/MoreVert";
import Tooltip from "@material-ui/core/Tooltip";
import Snackbar from "@material-ui/core/Snackbar";
import { format } from "date-fns";
import locale from "date-fns/locale/pt-BR";
import Grid from "@material-ui/core/Grid";

import Noty from "~/components/noty-alert";
import api from "~/services/api";
import { styles } from "~/pages/nps/styles/styles";
import ModalConfimation from "~/components/modals/modal-confirmation";
import DashCard from "~/pages/nps/components/dash-card";
import DashMasterCard from "./components/dash-marter-card";
import Table from "~/components/Tables/table";
import DashContentModal from "~/pages/nps/components/dash-content-modal";
import ContentModalReplica from "~/components/modals-content/replica-rel";
import { getUser } from "~/services/auth";

const topColumns = [
  "Franquia",
  "Respostas",
  "Promotor",
  "Neutro",
  "Detrator",
  "NPS"
];

const dtrColumns = [
  {
    name: "loja",
    label: "Loja",
    options: {
      display: false,
      viewColumns: false
    }
  },
  {
    name: "funcao",
    label: "Função",
    options: {
      display: false,
      viewColumns: false
    }
  },
  { name: "franquia", label: "Franquia" },
  { name: "quesito", label: "Quesito" },
  { name: "avaliador", label: "Avaliador" },
  {
    name: "justificativa",
    label: "Justificativa",
    options: {
      display: false
    }
  },
  {
    name: "replica",
    label: "Réplica",
    options: {
      display: false,
      viewColumns: false
    }
  },
  {
    name: "contato",
    label: "Contato",
    options: {
      display: false,
      viewColumns: false
    }
  },
  {
    name: "cadastro",
    label: "Cadastro",
    options: {
      display: false,
      viewColumns: false
    }
  },
  {
    name: "nota",
    label: "Menção",
    options: {
      customBodyRender: value => {
        return value === 0 ? (
          <div className="option0">{"Péssimo"}</div>
        ) : (
          <div className="option2">{"Ruim"}</div>
        );
      }
    }
  },
  {
    name: "retaguarda",
    label: "Retaguarda",
    options: {
      display: false,
      viewColumns: false
    }
  },
  {
    name: "codUsuarioAvaliacao",
    label: "Cod Usuário",
    options: {
      display: false,
      viewColumns: false
    }
  }
];

class Dashboard extends Component {
  state = {
    isLoading: true,
    franquia: [],
    idFranquia: null,
    nomeFranquia: "Geral",
    competencia: format(new Date(), "MMMM y", { locale }),
    periodo: new Date(),
    modal: {
      open: false,
      title: "Alterar Franquia"
    },
    alert: false,
    alertIcon: null,
    alertMessage: null,
    indicadores: [],
    npsChart: [],
    npsChartGeral: [],
    columns: dtrColumns,
    data: [],
    rowData: ""
  };

  async componentDidMount() {
    await this.getDashboard();
    this.getFranquias();
  }

  test = rowData => {
    if (rowData) {
      rowData = decodeURI(rowData.replace("?", ""));
      try {
        rowData = JSON.parse(rowData);
        console.log(rowData);
        this.setState({
          configDash: false,
          modal: {
            open: true,
            title: `${rowData[4]} - ${rowData[3]}`
          },
          mLoja: rowData[0],
          mAvaliador: rowData[4],
          mFuncao: rowData[1],
          mCodPergunta: rowData[3] === "Sistema" ? 2 : 1,
          mNota:
            rowData[9] === 0 ? (
              <div className="option0">{"Péssimo"}</div>
            ) : (
              <div className="option2">{"Ruim"}</div>
            ),
          mContatoVal: rowData[7][0],
          mContato: rowData[7],
          mCadastro: rowData[8],
          mJustificativa: rowData[5],
          mReplica: "",
          mRetaguarda: rowData[10],
          mCodAvaliador: rowData[11]
        });
      } catch (error) {}
    }
  };

  getFranquias = async () => {
    try {
      const response = await api.getFranquias();
      const franquia = response.data.map(item => ({
        value: item.idfranquia,
        label: item.nome
      }));
      this.setState({ franquia, isLoading: false });
    } catch (error) {
      let value =
        error.response === undefined
          ? "Serviço Temporariamente Indisponível."
          : error.response.data;
      this.setState({
        error: value,
        isLoading: false
      });
    }
  };

  handleChange = opt => event => {
    let value = null;
    let label = "Geral";

    if (event === null) {
      value = null;
    } else {
      value = event.value;
      label = event.label;
    }
    this.setState({
      [opt]: value,
      franquiaSel: label,
      isLoading: false
    });
  };

  handleChangeAlias = opt => event =>
    this.setState({ [opt]: event.target.value });

  openModal = () => {
    this.setState(prevState => ({
      modal: {
        open: true,
        title: "Alterar Franquia / Período"
      },
      configDash: true
    }));
  };

  handleCancel = () => {
    this.setState(prevState => ({
      modal: {
        open: false,
        title: prevState.modal.title
      }
    }));
  };

  getDashboard = async () => {
    try {
      this.setState({ isLoading: true });
      const { idFranquia, franquiaSel, periodo } = this.state;
      let date = format(periodo, "MM/y");

      const response = await api.getDashNps(idFranquia, date);

      const npsChart = response.data.periodosNps;
      const npsChartGeral = response.data.npsUnificado;
      const indicadores = response.data.indicadores;
      const npsdata = response.data.dadosNps.map(item => [
        item.retaguarda,
        item.numVotos,
        item.promotor,
        item.neutro,
        item.detrator,
        item.nps
      ]);
      const data = response.data.detratoresSemResposta.map(item => ({
        loja: item.loja,
        funcao: item.funcao,
        franquia: item.franquia,
        quesito: item.pergunta,
        avaliador: item.avaliador,
        justificativa: item.justificativa,
        replica: item.replica,
        contato: item.contato,
        cadastro: item.cadastro,
        nota: item.nota,
        retaguarda: item.retaguarda,
        codUsuarioAvaliacao: item.codUsuarioAvaliacao
      }));

      this.setState(prevState => ({
        modal: {
          open: false,
          title: prevState.modal.title
        },
        npsChart,
        npsChartGeral,
        topNps: { columns: topColumns, data: npsdata },
        data,
        indicadores,
        isLoading: false,
        nomeFranquia: franquiaSel === undefined ? "Geral" : franquiaSel,
        competencia: format(periodo, "MMMM y", { locale })
      }));
    } catch (error) {
      let value =
        error.response === undefined
          ? "Serviço Temporariamente Indisponível."
          : error.response.data;
      this.setState({
        isLoading: false,
        alert: true,
        alertIcon: "error",
        alertMessage: value
      });
    }
    console.log(this.state);

    this.test(this.props.location.search);
  };

  handleClose = reason => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ alert: false });
  };

  handleDateChange = () => date => this.setState({ periodo: date });

  openDatails = (rowData, rowMeta) => {
    console.log(rowData);
    this.setState({
      configDash: false,
      modal: {
        open: true,
        title: `${rowData[4]} - ${rowData[3]}`
      },
      mLoja: rowData[0],
      mAvaliador: rowData[4],
      mFuncao: rowData[1],
      mCodPergunta: rowData[3] === "Sistema" ? 2 : 1,
      mNota: rowData[9],
      mContatoVal: rowData[7][0],
      mContato: rowData[7],
      mCadastro: rowData[8],
      mJustificativa: rowData[5],
      mReplica: "",
      mRetaguarda: rowData[10],
      mCodAvaliador: rowData[11]
    });
    console.log(this.state);
  };

  handlePostReply = async () => {
    // this.setState({ isLoading: true });
    const UsuarioCigam = await atob(getUser());
    const {
      mRetaguarda,
      mCodAvaliador,
      mCodPergunta,
      mCadastro,
      mReplica
    } = this.state;

    const replica = {
      Retaguarda: mRetaguarda,
      CodUsuarioAvaliacao: mCodAvaliador,
      CodNpsPergunta: mCodPergunta,
      DatAvaliacao: mCadastro,
      UsuarioCigam,
      DscReplica: mReplica
    };

    console.log(replica);
    try {
      const response = await api.postReply(replica);
      if (response.status === 200) {
        this.setState(prevState => ({
          modal: {
            open: false,
            title: prevState.modal.title
          },
          alert: true,
          alertIcon: "success",
          alertMessage: "Resposta Cadastrada com Sucesso!"
        }));
        await this.getDashboard();
      }
      this.setState({ isLoading: false });
    } catch (error) {
      let value =
        error.response === undefined
          ? "Serviço Temporariamente Indisponível."
          : error.response.data;
      this.setState({
        isLoading: false,
        alert: true,
        alertIcon: "error",
        alertMessage: value
      });
    }
  };

  render() {
    const { classes } = this.props;
    const states = this.state;

    return (
      <>
        {states.isLoading && <CircularProgress className={classes.loader} />}
        {!states.isLoading && (
          <Grid
            item
            container
            direction="row"
            alignItems="center"
            justify="space-between"
          >
            <Typography variant="h5">
              {states.nomeFranquia} - {states.competencia}
            </Typography>

            <Tooltip
              title="Alterar"
              placement="left"
              onClick={() => this.openModal()}
            >
              <Fab size="small">
                <MoreVert />
              </Fab>
            </Tooltip>
          </Grid>
        )}

        {!states.isLoading && (
          <Grid container spacing={2}>
            {states.indicadores.map(item => (
              <Grid item xs={11} sm={12} lg={6} key={item.codNpsPergunta}>
                <DashMasterCard title={item.dscNpsPergunta}>
                  <Grid container spacing={2}>
                    <DashCard
                      xs={6}
                      sm={6}
                      md={3}
                      color="success"
                      ico="far fa-thumbs-up"
                      foot={`${item.qtdPromotor} votos.`}
                    >
                      <Typography variant="h6" align="center">
                        {`${item.percPromotor}%`}
                      </Typography>
                    </DashCard>
                    <DashCard
                      xs={6}
                      sm={6}
                      md={3}
                      color="warning"
                      ico="neutro"
                      foot={`${item.qtdNeutro} votos.`}
                    >
                      <Typography variant="h6" align="center">
                        {`${item.percNeutro}%`}
                      </Typography>
                    </DashCard>
                    <DashCard
                      xs={6}
                      sm={6}
                      md={3}
                      color="danger"
                      ico="far fa-thumbs-down"
                      foot={`${item.qtdDetrator} votos.`}
                    >
                      <Typography variant="h6" align="center">
                        {`${item.percDetrator}%`}
                      </Typography>
                    </DashCard>
                    <DashCard
                      xs={6}
                      sm={6}
                      md={3}
                      color="info"
                      ico="fas fa-tachometer-alt"
                      foot="NPS"
                    >
                      <Typography variant="h6" align="center">
                        {item.nps}
                      </Typography>
                    </DashCard>
                  </Grid>
                </DashMasterCard>
              </Grid>
            ))}

            <DashCard md={12} color="success" ico="fas fa-table">
              <Table
                title="Top NPS / Franquia Unificado"
                columns={states.topNps.columns}
                data={states.topNps.data}
                states={states}
                options={{
                  display: false,
                  download: false,
                  filter: false,
                  print: false,
                  viewColumns: false,
                  responsive: "scroll",
                  selectableRows: false,
                  rowsPerPage: 5,
                  rowsPerPageOptions: [5, 10, 20, 100]

                  // onRowClick: rowData => this.openDatails(rowData)
                }}
              />
            </DashCard>

            <DashCard md={12} lg={6} color="info" ico="fas fa-chart-line">
              <Chart
                chartType="LineChart"
                loader={<CircularProgress className={classes.loader} />}
                data={states.npsChart}
                options={{
                  title: "NPS Geral",
                  // curveType: "function",
                  legend: { position: "top" }
                  // colors: ["#882520", "#b01200"]
                }}
              />
            </DashCard>

            <DashCard md={12} lg={6} color="info" ico="far fa-chart-bar">
              <Chart
                chartType="Bar"
                loader={<CircularProgress className={classes.loader} />}
                data={states.npsChartGeral}
                options={{
                  chart: {
                    title: "NPS Geral Unificado",
                    subtitle: "Sistema / Suporte"
                  },
                  legend: { position: "none" }
                }}
              />
            </DashCard>

            <DashCard md={12} color="danger" ico="fas fa-table">
              <Table
                title="Detratores sem resposta"
                columns={states.columns}
                data={states.data}
                states={states}
                options={{
                  display: false,
                  download: false,
                  filter: false,
                  print: false,
                  responsive: "scroll",
                  selectableRows: false,
                  rowsPerPage: 10,
                  rowsPerPageOptions: [20, 50, 100],
                  onRowClick: rowData => this.openDatails(rowData)
                }}
              />
            </DashCard>
          </Grid>
        )}

        <ModalConfimation
          overflow="ofVisible"
          maxWidth="md"
          states={states}
          visible={states.modal.open}
          bts={["Cancelar", `${states.configDash ? "Buscar" : "Salvar"}`]}
          handleCancel={() => this.handleCancel()}
          handleConfirm={() =>
            states.configDash ? this.getDashboard() : this.handlePostReply()
          }
        >
          {states.configDash && (
            <DashContentModal
              states={states}
              handleChange={opt => this.handleChange(opt)}
              handleDateChange={() => this.handleDateChange()}
            />
          )}

          {!states.configDash && (
            <ContentModalReplica
              states={states}
              handleChangeContato={this.handleChangeAlias("mContatoVal")}
              handleChangeReplica={this.handleChangeAlias("mReplica")}
            />
          )}
        </ModalConfimation>

        <Snackbar
          anchorOrigin={{
            vertical: "top",
            horizontal: "right"
          }}
          open={states.alert}
          autoHideDuration={3000}
          onClose={this.handleClose}
        >
          <Noty
            onClose={this.handleClose}
            variant={states.alertIcon}
            message={states.alertMessage}
          />
        </Snackbar>
      </>
    );
  }
}

export default withStyles(styles)(Dashboard);
