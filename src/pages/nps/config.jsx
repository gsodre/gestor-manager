import React, { Component } from "react";
import classNames from "classnames";
import { withStyles } from "@material-ui/core/styles";
import Snackbar from "@material-ui/core/Snackbar";
import CircularProgress from "@material-ui/core/CircularProgress";
import Icon from "@material-ui/core/Icon";

import { styles } from "~/pages/nps/styles/styles";
import FilterCard from "~/components/filter-card";
import Table from "~/components/Tables/table";
import api from "~/services/api";
import ModalConfimation from "~/components/modal-confirmation";
import Noty from "~/components/noty-alert";

const status = [
  [
    { value: "T", text: "Todos" },
    { value: "A", text: "Ativos" },
    { value: "I", text: "Inativos" }
  ],
  [
    { value: 1, text: "Ativo" },
    { value: 0, text: "Inativo" }
  ]
];

const days = [
  { value: 0, text: "Sem Bloqueio" },
  { value: 1, text: "1 dia" },
  { value: 2, text: "2 dias" },
  { value: 3, text: "3 dias" },
  { value: 4, text: "4 dias" },
  { value: 5, text: "5 dias" },
  { value: 6, text: "6 dias" },
  { value: 7, text: "7 dias" },
  { value: 8, text: "8 dias" },
  { value: 9, text: "9 dias" },
  { value: 10, text: "10 dias" },
  { value: 11, text: "11 dias" },
  { value: 12, text: "12 dias" },
  { value: 13, text: "13 dias" },
  { value: 14, text: "14 dias" },
  { value: 15, text: "15 dias" },
  { value: 16, text: "16 dias" },
  { value: 17, text: "17 dias" },
  { value: 18, text: "18 dias" },
  { value: 19, text: "19 dias" },
  { value: 20, text: "20 dias" },
  { value: 21, text: "21 dias" },
  { value: 22, text: "22 dias" },
  { value: 23, text: "23 dias" },
  { value: 24, text: "24 dias" },
  { value: 25, text: "25 dias" },
  { value: 26, text: "26 dias" },
  { value: 27, text: "27 dias" },
  { value: 28, text: "28 dias" },
  { value: 29, text: "29 dias" },
  { value: 30, text: "30 dias" }
];

const score = [
  { value: 0, text: "Nota 0" },
  { value: 1, text: "Nota 1" },
  { value: 2, text: "Nota 2" },
  { value: 3, text: "Nota 3" },
  { value: 4, text: "Nota 4" },
  { value: 5, text: "Nota 5" },
  { value: 6, text: "Nota 6" },
  { value: 7, text: "Nota 7" },
  { value: 8, text: "Nota 8" },
  { value: 9, text: "Nota 9" },
  { value: 10, text: "Nota 10" }
];

class Config extends Component {
  state = {
    isLoading: true,
    alert: false,
    alertIcon: "",
    alertMessage: "",
    franquia: [],
    idFranquia: null,
    status: "T",
    result: false,
    columns: [
      {
        name: "#",
        options: {
          viewColumns: false,
          display: false
        }
      },
      "Loja",
      {
        name: "Dias para Bloqueio",
        options: {
          customBodyRender: value => {
            return value === 0 ? <p>Sem Bloqueio</p> : +value;
            // return updateValue;
          }
        }
      },
      "Nota Obriga Justificativa",
      {
        name: "Status",
        options: {
          customBodyRender: (value, updateValue) => {
            return value === 1 ? (
              <p className="success">
                {"Ativo"}
                <Icon
                  className={classNames("icons-table", "far fa-check-circle")}
                />
              </p>
            ) : (
              <p className="danger">
                {"Inativo"}
                <Icon
                  className={classNames("icons-table", "far fa-times-circle")}
                />
              </p>
            );
          }
        }
      }
    ],
    data: [],
    modal: {
      open: false,
      title: "",
      content: []
    },
    codNps: 0,
    days: 1,
    score: 6,
    modalStatus: 1
  };

  componentDidMount() {
    this.getFranquias();
  }

  handleClear = () => {
    this.setState({
      result: false,
      status: "T"
    });
  };

  openModal = () => {
    this.setState(prevState => ({
      modal: {
        open: true,
        title: prevState.modal.title,
        content: prevState.modal.content
      }
    }));
  };

  handleSaveConfig = async () => {
    this.setState({ isLoading: true });
    const states = this.state;
    const config = {
      codNpsConfiguracao: states.codNps,
      loja: states.modal.title,
      numDiasBloqueio: states.days,
      numNotaObrigaJustif: states.score,
      flgAtivo: states.modalStatus,
      modificado: -1
    };
    try {
      const response = await api.putConfig(config);
      if (response.status === 200) {
        this.setState(prevState => ({
          modal: {
            open: false,
            title: prevState.modal.title,
            content: prevState.modal.content
          },
          alert: true,
          alertIcon: "success",
          alertMessage: "Configuração Alterada com Sucesso!"
        }));
        this.getData();
      }
      this.setState({ isLoading: false });
    } catch (error) {
      let value =
        error.response === undefined
          ? "Serviço Temporariamente Indisponível."
          : error.response.data;
      this.setState({
        error: value,
        isLoading: false
      });
    }
  };

  handleCancel = () => {
    this.setState(prevState => ({
      modal: {
        open: false,
        title: prevState.modal.title,
        content: prevState.modal.content
      }
    }));
  };

  getFranquias = async () => {
    try {
      const response = await api.getFranquias();
      const franquia = response.data.map(item => ({
        value: item.idfranquia,
        label: item.nome
      }));
      this.setState({ franquia, isLoading: false });
    } catch (error) {
      let value =
        error.response === undefined
          ? "Serviço Temporariamente Indisponível."
          : error.response.data;
      this.setState({
        isLoading: false,
        alert: true,
        alertIcon: "error",
        alertMessage: value
      });
    }
  };

  getData = () => {
    if (this.state.idFranquia !== null) {
      this.setState({ isLoading: true });
      this.xhrRequest().then(response => {
        const data = response.data.map(item => [
          item.codNpsConfiguracao,
          item.loja,
          item.numDiasBloqueio,
          item.numNotaObrigaJustif,
          item.flgAtivo
        ]);
        data.length > 0
          ? this.setState({ data, result: true, isLoading: false })
          : this.setState({
              isLoading: false,
              result: false,
              alert: true,
              alertIcon: "warning",
              alertMessage: "Sua pesquisa não gerou resultados!"
            });
      });
    } else {
      this.setState({
        alert: true,
        alertIcon: "warning",
        alertMessage: "Selecione uma Franquia!"
      });
    }
  };

  xhrRequest = () =>
    new Promise((resolve, reject) => {
      const { idFranquia, status } = this.state;
      const response = api.getConfig(idFranquia, status);

      resolve(response);
    });

  handleChange = opt => event => {
    let value = null;
    event === null
      ? (value = null)
      : (value = opt === "idFranquia" ? event.value : event.target.value);
    this.setState({
      [opt]: value
    });
  };

  handleClose = reason => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ alert: false });
  };

  render() {
    const { classes } = this.props;
    const states = this.state;

    return (
      <div>
        {states.isLoading && <CircularProgress className={classes.loader} />}
        <FilterCard
          states={states}
          classes={classes}
          options={{ status }}
          title="Configurações"
          btAction={{ ico: "fa fa-search", text: "Pesquisar" }}
          handleChange={opt => this.handleChange(opt)}
          getData={() => this.getData()}
          handleClear={() => this.handleClear()}
        />
        {states.result && (
          <Table
            states={states}
            title="Listagem de configurações"
            columns={states.columns}
            data={states.data}
            options={{
              display: false,
              filter: false,
              print: false,
              responsive: "scroll",
              selectableRows: false,
              rowsPerPage: 50,
              rowsPerPageOptions: [50, 100, 200],
              onRowClick: rowData => {
                this.setState({
                  modal: {
                    open: true,
                    title: rowData[1],
                    content: rowData
                  },
                  codNps: rowData[0],
                  days: typeof rowData[2] === "object" ? 0 : rowData[2],
                  score: rowData[3],
                  modalStatus: rowData[4].props.children[0] === "Ativo" ? 1 : 0
                });
              }
            }}
          />
        )}

        <ModalConfimation
          maxWidth="md"
          options={{ days, status, score }}
          classes={classes}
          states={states}
          bts={["Cancelar", "Salvar"]}
          handleCancel={() => this.handleCancel()}
          handleChange={opt => this.handleChange(opt)}
          handleConfirm={() => this.handleSaveConfig()}
        />

        <Snackbar
          anchorOrigin={{
            vertical: "top",
            horizontal: "right"
          }}
          open={states.alert}
          autoHideDuration={3000}
          onClose={this.handleClose}
        >
          <Noty
            onClose={this.handleClose}
            variant={states.alertIcon}
            message={states.alertMessage}
          />
        </Snackbar>
      </div>
    );
  }
}

export default withStyles(styles)(Config);
