import React, { Component } from "react";
import { format, isBefore, lastDayOfMonth } from "date-fns";
import locale from "date-fns/locale/pt-BR";
import Grid from "@material-ui/core/Grid";
import Snackbar from "@material-ui/core/Snackbar";
import CircularProgress from "@material-ui/core/CircularProgress";
import TextField from "@material-ui/core/TextField";

import Noty from "~/components/noty-alert";
import api from "~/services/api";
import FilterCard from "~/components/cards/filter-card";
import LiveSearch from "~/components/auto-complete";
import Select from "~/components/select-simple";
import Table from "~/components/Tables/table";
import DatePicker from "~/components/FilterCards/date-picker";
import ModalConfimation from "~/components/modals/modal-confirmation";
import ContentModalReplica from "~/components/modals-content/replica-rel";
import { getUser } from "~/services/auth";

const nps = [
  { value: "T", text: "Todos" },
  { value: "D", text: "Detratores" },
  { value: "N", text: "Neutros" },
  { value: "P", text: "Promotores" }
];

const quesito = [
  { value: 0, text: "Todos" },
  { value: 2, text: "Sistema" },
  { value: 1, text: "Suporte" }
];

const comReplica = [
  { value: "T", text: "Todos" },
  { value: "S", text: "Com Réplica" },
  { value: "N", text: "Sem Réplica" }
];

const columns = [
  { name: "franquia", label: "Franquia" },
  { name: "loja", label: "Loja" },
  { name: "avaliador", label: "Avaliador" },
  { name: "funcao", label: "Função" },
  {
    name: "nota",
    label: "Menção",
    options: {
      display: false,
      customBodyRender: value => {
        return value === 0 ? (
          <div className="option0">{"Péssimo"}</div>
        ) : value === 2 ? (
          <div className="option2">{"Ruim"}</div>
        ) : value === 7 ? (
          <div className="option7">{"Regular"}</div>
        ) : value === 9 ? (
          <div className="option9">{"Bom"}</div>
        ) : (
          <div className="option10">{"Ótimo"}</div>
        );
      }
    }
  },
  {
    name: "nps",
    label: "NPS",
    options: {
      customBodyRender: value => {
        return value === "Promotor" ? (
          <p className="tab-promotor">{"Promotor"}</p>
        ) : value === "Detrator" ? (
          <p className="tab-detrator">{"Detrator"}</p>
        ) : (
          <p className="tab-neutro">{"Neutro"}</p>
        );
      }
    }
  },
  {
    name: "quesito",
    label: "Quesito",
    options: {
      download: false,
      customBodyRender: value => {
        return value === 2 ? <p>{"Sistema"}</p> : <p>{"Suporte"}</p>;
      }
    }
  },
  {
    name: "pergunta",
    label: "Pergunta",
    options: {
      viewColumns: false,
      display: false
    }
  },
  { name: "competencia", label: "Competência" },
  {
    name: "contato",
    label: "Contato",
    options: {
      viewColumns: false,
      display: false
    }
  },
  {
    name: "justificativa",
    label: "Justificativa",
    options: {
      display: false
    }
  },
  {
    name: "replica",
    label: "Réplica",
    options: {
      display: false
    }
  },
  {
    name: "usuarioReplica",
    label: "Usuário Réplica",
    options: {
      // viewColumns: false,
      // download: false,
      display: false
    }
  },
  {
    name: "codUsuario",
    label: "Cod. Usuário",
    options: {
      viewColumns: false,
      download: false,
      display: false
    }
  },
  {
    name: "cadastro",
    label: "Cadastro",
    options: {
      viewColumns: false,
      display: false,
      download: false
    }
  },
  {
    name: "cod",
    label: "Cod",
    options: {
      viewColumns: false,
      display: false,
      download: false
    }
  },
  {
    name: "codAvaliador",
    label: "Cod. Avaliador",
    options: {
      viewColumns: false,
      display: false,
      download: false
    }
  },
  {
    name: "retaguarda",
    label: "Retaguarda",
    options: {
      viewColumns: false,
      display: false,
      download: false
    }
  }
];

class Report extends Component {
  state = {
    isLoading: true,
    result: false,
    franquia: [],
    idFranquia: null,
    loja: [],
    idLoja: null,
    dateStart: new Date(),
    dateEnd: new Date(),
    nps: "T",
    quesito: 0,
    comReplica: "T",
    funcaoFiltro: "",
    alert: false,
    alertIcon: null,
    alertMessage: null,
    columns,
    data: [],
    modal: {
      open: false,
      title: ""
    },
    // reply: true,
    // btReply: true,
    pergunta: null,
    retaguarda: null,
    mLoja: null,
    avaliador: null,
    codAvaliador: null,
    funcao: null,
    nota: null,
    contato: null,
    justificativa: "",
    replica: "",
    usuario: null,
    cadastro: null,
    codPergunta: null,
    codUsuarioLogado: null
  };

  async componentDidMount() {
    await this.getFranquias();
  }

  handleClear = () => {
    this.setState({
      result: false,
      nps: "T",
      quesito: 0,
      dateStart: new Date(),
      dateEnd: new Date()
    });
  };

  getFranquias = async () => {
    try {
      const response = await api.getFranquias();
      const franquia = response.data.map(item => ({
        value: item.idfranquia,
        label: item.nome
      }));
      this.setState({ franquia, isLoading: false });
    } catch (error) {
      let value =
        error.response === undefined
          ? "Serviço Temporariamente Indisponível."
          : error.response.data;
      this.setState({
        isLoading: false,
        alert: true,
        alertIcon: "error",
        alertMessage: value
      });
    }
  };

  getLojas = async idFranquia => {
    this.setState({ isLoading: true });
    try {
      const response = await api.getLojas(idFranquia);
      const loja = response.data.map(item => ({
        value: item.idLoja,
        label: `${item.identificacaoLoja} - ${item.nome}`
      }));
      this.setState({ loja, isLoading: false });
    } catch (error) {
      let value =
        error.response === undefined
          ? "Serviço Temporariamente Indisponível."
          : error.response.data;
      this.setState({
        isLoading: false,
        alert: true,
        alertIcon: "error",
        alertMessage: value
      });
    }
  };

  handleChange = opt => event => {
    let value = null;
    event === null
      ? (value = null)
      : (value =
          opt === "idFranquia" || opt === "idLoja"
            ? event.value
            : event.target.value);
    this.setState({
      [opt]: value
    });
    value !== null && opt === "idFranquia"
      ? this.getLojas(value)
      : this.setState({ isLoading: false });
  };

  handleDateChange = opt => date => this.setState({ [opt]: date });

  handleClose = reason => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ alert: false });
  };

  getData = async () => {
    try {
      const {
        idFranquia,
        idLoja,
        dateStart,
        dateEnd,
        nps,
        quesito,
        comReplica,
        funcaoFiltro
      } = this.state;

      if (!isBefore(dateEnd, dateStart)) {
        this.setState({ isLoading: true });

        let dtInicial = format(dateStart, "y-MM-01");
        let dtFim = lastDayOfMonth(dateEnd);
        // let dtFim = format(dateEnd, "y-MM-30");

        const response = await api.getReport(
          idFranquia,
          idLoja,
          dtInicial,
          dtFim,
          nps,
          quesito,
          comReplica,
          funcaoFiltro
        );

        console.log(response.data);

        const data = response.data.map(item => ({
          franquia: item.nomeFranquia,
          loja: item.loja,
          avaliador: item.nomAvaliador,
          funcao: item.dscFuncaoAvaliador,
          nota: item.numNota,
          nps: item.classificacao,
          quesito: item.codNpsPergunta,
          pergunta: item.dscNpsPergunta,
          competencia: item.dataAvaliacao,
          contato: item.telefoneLoja,
          justificativa: item.dscjustificativa,
          replica: item.dscReplica,
          usuarioReplica: item.nomUsuarioCigam,
          codUsuario: item.codUsuarioCigam,
          cadastro: item.datAvaliacao,
          cod: item.codNpsPergunta,
          codAvaliador: item.codAvaliador,
          retaguarda: item.retaguarda
        }));
        data.length > 0
          ? this.setState({ data, result: true, isLoading: false })
          : this.setState({
              isLoading: false,
              result: false,
              alert: true,
              alertIcon: "warning",
              alertMessage: "Sua pesquisa não gerou resultados!"
            });
      } else {
        this.setState({
          alert: true,
          alertIcon: "warning",
          alertMessage: "Período incompatível."
        });
      }
    } catch (error) {
      let value =
        error.response === undefined
          ? "Serviço Temporariamente Indisponível."
          : error.response.data;
      this.setState({
        isLoading: false,
        alert: true,
        alertIcon: "error",
        alertMessage: value
      });
    }
  };

  handleCancel = () => {
    this.setState(prevState => ({
      modal: {
        open: false,
        title: prevState.modal.title,
        content: prevState.modal.content
      }
    }));
  };

  handlePostReply = async () => {
    this.setState({ isLoading: true });
    const UsuarioCigam = await atob(getUser());

    const {
      mRetaguarda,
      mCodAvaliador,
      mCodPergunta,
      mCadastro,
      mReplica
    } = this.state;

    const replica = {
      Retaguarda: mRetaguarda,
      CodUsuarioAvaliacao: mCodAvaliador,
      CodNpsPergunta: mCodPergunta,
      DatAvaliacao: mCadastro,
      UsuarioCigam,
      DscReplica: mReplica
    };

    try {
      const response = await api.postReply(replica);
      if (response.status === 200) {
        this.setState(prevState => ({
          modal: {
            open: false,
            title: prevState.modal.title
          },
          alert: true,
          alertIcon: "success",
          alertMessage: "Resposta Cadastrada com Sucesso!"
        }));
        this.getData();
      }
      this.setState({ isLoading: false });
    } catch (error) {
      let value =
        error.response === undefined
          ? "Serviço Temporariamente Indisponível."
          : error.response.data;
      this.setState({
        isLoading: false,
        alert: true,
        alertIcon: "error",
        alertMessage: value
      });
    }
  };

  openDatails = rowData => {
    console.log(rowData);
    this.setState({
      modal: {
        open: true,
        title: rowData[7]
      },
      mLoja: rowData[1],
      mAvaliador: rowData[2],
      mFuncao: rowData[3],
      // mCodPergunta: rowData[15] === "Sistema" ? 2 : 1,
      mCodPergunta: rowData[15],
      mNota: rowData[4],
      mContatoVal: rowData[9][0],
      mContato: rowData[9],
      mCadastro: rowData[14],
      mJustificativa:
        rowData[10] === (null || "") ? "Não Justificado." : rowData[10],
      mReplica: rowData[11] === null ? "" : rowData[11],
      // mReplica: rowData[11],
      mRetaguarda: rowData[17],
      mCodAvaliador: rowData[16]
    });
  };

  render() {
    const { classes } = this.props;
    const states = this.state;
    return (
      <div>
        {states.isLoading && <CircularProgress className="loader" />}

        <FilterCard
          title="Relatório NPS"
          states={states}
          handleClear={() => this.handleClear()}
          getData={() => this.getData()}
          btAction={{ ico: "fa fa-search", text: "Pesquisar" }}
        >
          <Grid container justify="center" spacing={1}>
            <LiveSearch
              isDisabled={states.isLoading}
              placeholder="Todas"
              label="Franquia"
              states={states}
              grid={{ xs: 6, md: 3 }}
              options={states.franquia}
              onChange={this.handleChange("idFranquia")}
            />

            <LiveSearch
              isDisabled={states.isLoading || states.idFranquia === null}
              placeholder="Todas"
              label="Loja"
              states={states}
              grid={{ xs: 6, md: 3 }}
              options={states.loja}
              onChange={this.handleChange("idLoja")}
            />

            <DatePicker
              label="Período De"
              views={["year", "month"]}
              states={states}
              value={states.dateStart}
              classes={classes}
              grid={{ xs: 6, md: 2 }}
              maxDate={new Date()}
              onChange={this.handleDateChange("dateStart")}
            />

            <DatePicker
              label="Até"
              views={["year", "month"]}
              states={states}
              value={states.dateEnd}
              classes={classes}
              grid={{ xs: 6, md: 2 }}
              maxDate={new Date()}
              onChange={this.handleDateChange("dateEnd")}
            />
          </Grid>

          <Grid container justify="center" spacing={1}>
            <Select
              label="NPS"
              disabled={states.isLoading}
              states={states}
              value={states.nps}
              options={nps}
              classes={classes}
              grid={{ xs: 6, md: 2 }}
              onChange={this.handleChange("nps")}
            />
            <Select
              label="Quesito"
              disabled={states.isLoading}
              states={states}
              value={states.quesito}
              options={quesito}
              classes={classes}
              grid={{ xs: 6, md: 2 }}
              onChange={this.handleChange("quesito")}
            />

            <Select
              label="Réplica"
              disabled={states.isLoading}
              states={states}
              value={states.comReplica}
              options={comReplica}
              classes={classes}
              grid={{ xs: 6, md: 2 }}
              onChange={this.handleChange("comReplica")}
            />

            <Grid xs={6} md={2} item>
              <TextField
                disabled={states.isLoading}
                fullWidth
                label="Função"
                variant="outlined"
                value={states.funcaoFiltro}
                margin="dense"
                onChange={e => this.setState({ funcaoFiltro: e.target.value })}
              />
            </Grid>
          </Grid>
        </FilterCard>

        {states.result && (
          <Table
            title="Relatório"
            columns={states.columns}
            data={states.data}
            states={states}
            options={{
              display: false,
              filter: false,
              print: false,
              responsive: "scroll",
              selectableRows: false,
              rowsPerPage: 50,
              rowsPerPageOptions: [50, 100, 200],
              downloadOptions: {
                filename: `Relatorio_NPS_${format(states.dateStart, "MMMy", {
                  locale
                })}_a_${format(states.dateEnd, "MMMy", {
                  locale
                })}.csv`,
                separator: ";"
              },
              onDownload: (buildHead, buildBody, columns, data) => {
                return "\uFEFF" + buildHead(columns) + buildBody(data);
              },
              onRowClick: rowData => this.openDatails(rowData)
            }}
          />
        )}

        <ModalConfimation
          maxWidth="md"
          states={states}
          visible={true}
          bts={["Cancelar", "Salvar"]}
          handleCancel={() => this.handleCancel()}
          handleConfirm={() => this.handlePostReply()}
        >
          <ContentModalReplica
            states={states}
            handleChangeContato={this.handleChange("mContatoVal")}
            handleChangeReplica={this.handleChange("mReplica")}
          />
        </ModalConfimation>

        <Snackbar
          anchorOrigin={{
            vertical: "top",
            horizontal: "right"
          }}
          open={states.alert}
          autoHideDuration={3000}
          onClose={this.handleClose}
        >
          <Noty
            onClose={this.handleClose}
            variant={states.alertIcon}
            message={states.alertMessage}
          />
        </Snackbar>
      </div>
    );
  }
}

export default Report;
