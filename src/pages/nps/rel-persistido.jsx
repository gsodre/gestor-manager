import React, { Component } from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import Grid from "@material-ui/core/Grid";
import Snackbar from "@material-ui/core/Snackbar";

import api from "~/services/api";
import FilterCard from "~/components/cards/filter-card";
import LiveSearch from "~/components/auto-complete";
import Select from "~/components/select-simple";
import Table from "~/components/Tables/table";
import ModalConfimation from "~/components/modals/modal-confirmation";
import Noty from "~/components/noty-alert";
import ModalContent from "~/pages/nps/components/rel-persistidos-content-modal";

const persistencia = [
  { value: 2, text: "2 Meses" },
  { value: 3, text: "3 Meses" },
  { value: 4, text: "4 Meses" },
  { value: 5, text: "5 Meses" },
  { value: 6, text: "6 Meses" }
];

const nps = [
  { value: "D", text: "Detratores" },
  { value: "N", text: "Neutros" },
  { value: "P", text: "Promotores" }
];

const quesito = [
  { value: 0, text: "Todos" },
  { value: 2, text: "Sistema" },
  { value: 1, text: "Suporte" }
];

const columns = [
  "Franquia",
  "Loja",
  "Avaliador",
  {
    name: "NPS",
    options: {
      customBodyRender: value => {
        return value === "P" ? (
          <p className="tab-promotor">{"Promotor"}</p>
        ) : value === "D" ? (
          <p className="tab-detrator">{"Detrator"}</p>
        ) : (
          <p className="tab-neutro">{"Neutro"}</p>
        );
      }
    }
  },
  {
    name: "Quesito",
    options: {
      download: false,
      customBodyRender: value => {
        return value === 2 ? <p>{"Sistema"}</p> : <p>{"Suporte"}</p>;
      }
    }
  },
  {
    name: "Contato",
    options: {
      display: false,
      viewColumns: false
    }
  },
  {
    name: "Loja",
    options: {
      display: false,
      viewColumns: false
    }
  },
  {
    name: "Função",
    options: {
      // display: false
      // viewColumns: false
    }
  },
  {
    name: "NPS",
    options: {
      display: false,
      viewColumns: false
    }
  },
  {
    name: "Respostas",
    options: {
      display: false,
      viewColumns: false
    }
  }
];

class RelPersistido extends Component {
  state = {
    isLoading: true,
    result: false,
    franquia: [],
    idFranquia: null,
    loja: [],
    idLoja: null,
    persistencia: 2,
    nps: "D",
    quesito: 0,
    modal: {
      open: false,
      title: ""
    },
    columns,
    data: []
  };

  async componentDidMount() {
    await this.getFranquias();
  }

  handleClose = reason => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ alert: false });
  };

  getFranquias = async () => {
    try {
      const response = await api.getFranquias();
      const franquia = response.data.map(item => ({
        value: item.idfranquia,
        label: item.nome
      }));
      this.setState({ franquia, isLoading: false });
    } catch (error) {
      let value =
        error.response === undefined
          ? "Serviço Temporariamente Indisponível."
          : error.response.data;
      this.setState({
        isLoading: false,
        alert: true,
        alertIcon: "error",
        alertMessage: value
      });
    }
  };

  getLojas = async idFranquia => {
    this.setState({ isLoading: true });
    try {
      const response = await api.getLojas(idFranquia);
      const loja = response.data.map(item => ({
        value: item.identificacaoLoja,
        label: `${item.identificacaoLoja} - ${item.nome}`
      }));
      this.setState({ loja, isLoading: false });
    } catch (error) {
      let value =
        error.response === undefined
          ? "Serviço Temporariamente Indisponível."
          : error.response.data;
      this.setState({
        isLoading: false,
        alert: true,
        alertIcon: "error",
        alertMessage: value
      });
    }
  };

  handleChange = opt => event => {
    let value = null;
    event === null
      ? (value = null)
      : (value =
          opt === "idFranquia" || opt === "idLoja"
            ? event.value
            : event.target.value);
    this.setState({
      [opt]: value
    });
    value !== null && opt === "idFranquia"
      ? this.getLojas(value)
      : this.setState({ isLoading: false });
  };

  getData = async () => {
    try {
      this.setState({ isLoading: true });

      const { idFranquia, idLoja, persistencia, nps, quesito } = this.state;
      console.log(idFranquia, idLoja, persistencia, nps, quesito);
      const response = await api.getRelPersistidos(
        idFranquia,
        idLoja,
        persistencia,
        nps,
        quesito
      );

      const data = response.data.map(item => [
        item.franquia,
        item.loja,
        item.nome,
        nps,
        item.codNpsPergunta,
        item.contato,
        item.respostas[0].loja,
        item.respostas[0].dscFuncaoAvaliador,
        item.respostas[0].classificacao,
        JSON.stringify(item.respostas)
      ]);

      data.length > 0
        ? this.setState({ data, result: true, isLoading: false })
        : this.setState({
            isLoading: false,
            result: false,
            alert: true,
            alertIcon: "warning",
            alertMessage: "Sua pesquisa não gerou resultados!"
          });
    } catch (error) {
      let value =
        error.response === undefined
          ? "Serviço Temporariamente Indisponível."
          : error.response.data;
      this.setState({
        isLoading: false,
        alert: true,
        alertIcon: "error",
        alertMessage: value
      });
    }
  };

  openDatails = rowData => {
    console.log(rowData);
    this.setState({
      modal: {
        open: true,
        title: `${rowData[0]} - ${rowData[1]}`
      },
      mLoja: rowData[5],
      mAvaliador: rowData[1],
      mFuncao: rowData[6],
      mNps: rowData[7],
      mContato: rowData[4],
      mContatoVal: rowData[4][0],
      mRespostas: JSON.parse(rowData[8])
    });
    console.log(this.state);
  };

  handleCancel = () => {
    this.setState(prevState => ({
      modal: {
        open: false,
        title: prevState.modal.title,
        content: prevState.modal.content
      }
    }));
  };

  render() {
    const { classes } = this.props;
    const states = this.state;
    return (
      <>
        {states.isLoading && <CircularProgress className="loader" />}

        <FilterCard
          title="Relatório Persistidos"
          states={states}
          handleClear={() => this.handleClear()}
          getData={() => this.getData()}
          btAction={{ ico: "fa fa-search", text: "Pesquisar" }}
        >
          <Grid container justify="center" spacing={1}>
            <LiveSearch
              isDisabled={states.isLoading}
              placeholder="Todas"
              label="Franquia"
              states={states}
              grid={{ xs: 6, md: 3 }}
              options={states.franquia}
              onChange={this.handleChange("idFranquia")}
            />

            <LiveSearch
              isDisabled={states.isLoading || states.idFranquia === null}
              placeholder="Todas"
              label="Loja"
              states={states}
              grid={{ xs: 6, md: 3 }}
              options={states.loja}
              onChange={this.handleChange("idLoja")}
            />

            <Select
              label="Persistência"
              states={states}
              value={states.persistencia}
              options={persistencia}
              classes={classes}
              grid={{ xs: 6, md: 2 }}
              onChange={this.handleChange("persistencia")}
            />

            <Select
              label="NPS"
              states={states}
              value={states.nps}
              options={nps}
              classes={classes}
              grid={{ xs: 6, md: 2 }}
              onChange={this.handleChange("nps")}
            />
            <Select
              states={states}
              label="Quesito"
              value={states.quesito}
              options={quesito}
              classes={classes}
              grid={{ xs: 6, md: 2 }}
              onChange={this.handleChange("quesito")}
            />
          </Grid>
        </FilterCard>

        {states.result && (
          <Table
            title="Relatório de Persistidos"
            columns={states.columns}
            data={states.data}
            states={states}
            options={{
              display: false,
              filter: false,
              print: false,
              download: false,
              // viewColumns: false,
              responsive: "scroll",
              selectableRows: false,
              rowsPerPage: 10,
              rowsPerPageOptions: [10, 50, 100, 200],
              downloadOptions: {
                filename: `Relatorio_Persistidos_${
                  states.persistencia
                }meses}.csv`,
                separator: ";"
              },
              onDownload: (buildHead, buildBody, columns, data) => {
                return "\uFEFF" + buildHead(columns) + buildBody(data);
              },
              onRowClick: rowData => this.openDatails(rowData)
            }}
          />
        )}

        <ModalConfimation
          maxWidth="md"
          states={states}
          visible={false}
          bts={["Fechar"]}
          handleCancel={() => this.handleCancel()}
          handleConfirm={() => this.handlePostReply()}
        >
          <ModalContent
            states={states}
            handleChange={this.handleChange("mContatoVal")}
          />
        </ModalConfimation>

        <Snackbar
          anchorOrigin={{
            vertical: "top",
            horizontal: "right"
          }}
          open={states.alert}
          autoHideDuration={3000}
          onClose={this.handleClose}
        >
          <Noty
            onClose={this.handleClose}
            variant={states.alertIcon}
            message={states.alertMessage}
          />
        </Snackbar>
      </>
    );
  }
}

export default RelPersistido;
