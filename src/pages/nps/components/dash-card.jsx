import React from "react";
import Icon from "@material-ui/core/Icon";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";

import Card from "~/components/cards/card";
import CardHeader from "~/components/cards/card-header";
import CardIcon from "~/components/cards/card-icon";
import CardFooter from "~/components/cards/card-footer";
import CardBody from "~/components/cards/card-body";

function DashCard({ ...props }) {
  const { pie, ico, color, children, foot, ...rest } = props;
  return (
    <Grid item {...rest}>
      <Card>
        <CardHeader icon>
          <CardIcon color={color}>
            {ico === "neutro" ? (
              <Icon>thumbs_up_down</Icon>
            ) : (
              <Icon className={ico} />
            )}
          </CardIcon>
        </CardHeader>
        <CardBody className={pie}>{children}</CardBody>
        {foot && (
          <CardFooter stats>
            <Typography variant="caption">{foot}</Typography>
          </CardFooter>
        )}
      </Card>
    </Grid>
  );
}
export default DashCard;
