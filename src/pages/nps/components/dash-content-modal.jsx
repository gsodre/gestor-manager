import React from "react";
import Grid from "@material-ui/core/Grid";

import LiveSearch from "~/components/auto-complete";
import DatePicker from "~/components/FilterCards/date-picker";

const DashContentModal = ({ states, handleChange, handleDateChange }) => (
  <Grid container justify="center" spacing={1}>
    <LiveSearch
      isDisabled={states.isLoading}
      placeholder="Todas"
      label="Franquia"
      states={states}
      grid={{ xs: 12, md: 6 }}
      options={states.franquia}
      onChange={handleChange("idFranquia")}
    />

    <DatePicker
      label="Período"
      // format="dd/MM/yyyy"
      views={["year", "month"]}
      states={states}
      value={states.periodo}
      // classes={classes}
      grid={{ xs: 12, md: 4 }}
      maxDate={new Date()}
      onChange={handleDateChange()}
    />
    {/* <LiveSearch
      isDisabled={states.idFranquia === null}
      placeholder="Todas"
      label="Loja"
      states={states}
      grid={{ xs: 12, md: 6 }}
      options={states.loja}
      onChange={handleChange("idLoja")}
    /> */}
  </Grid>
);

export default DashContentModal;
