import React from "react";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";

import Card from "~/components/cards/card";
import CardHeader from "~/components/cards/card-header";
import CardBody from "~/components/cards/card-body";

function DashMasterCard({ ...props }) {
  const { title, ico, color, children, ...rest } = props;
  return (
    <Grid item {...rest}>
      <Card className="master-dash-nps">
        <CardHeader>
          <Typography variant="h6">{title}</Typography>
        </CardHeader>
        <CardBody className="body-card">{children}</CardBody>
      </Card>
    </Grid>
  );
}
export default DashMasterCard;
