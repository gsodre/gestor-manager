import React from "react";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
// import Fab from "@material-ui/core/Fab";
// import ReplyIcon from "@material-ui/icons/Reply";
// import Tooltip from "@material-ui/core/Tooltip";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import MenuItem from "@material-ui/core/MenuItem";

const Content = ({ states, handleReply, handleChange }) => (
  <Grid container spacing={1}>
    <Table size="small">
      <TableHead>
        <TableRow>
          <TableCell>Loja</TableCell>
          <TableCell>Avaliador</TableCell>
          <TableCell>Função</TableCell>
          <TableCell>Nota</TableCell>
          <TableCell>Contato</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        <TableRow>
          <TableCell>{states.mLoja}</TableCell>
          <TableCell>{states.mAvaliador}</TableCell>
          <TableCell>{states.mFuncao}</TableCell>
          <TableCell>{states.mNps}</TableCell>
          <TableCell>
            {states.mContato !== undefined && states.mContato.length > 0 ? (
              <TextField
                select
                fullWidth
                value={states.mContatoVal}
                onChange={handleChange}
              >
                {states.mContato.map(item => (
                  <MenuItem value={item} key={item}>
                    {item}
                  </MenuItem>
                ))}
              </TextField>
            ) : (
              "Sem Número"
            )}
          </TableCell>
        </TableRow>
      </TableBody>
    </Table>

    {states.mRespostas !== undefined &&
      states.mRespostas.map((item, index) => (
        <>
          {/* <Grid xs={12} md={6} item> */}
          <TextField
            key={index}
            // disabled={true}
            label={`Justificativa de ${item.dataAvaliacao}`}
            multiline
            fullWidth
            rows="3"
            value={item.dscjustificativa}
            margin="normal"
            variant="outlined"
          />
          {/* </Grid> */}

          {/* <Grid xs={12} md={6} item> */}
          <TextField
            key={index}
            // disabled={true}
            label="Réplica"
            multiline
            fullWidth
            rows="3"
            value={item.dscReplica === null ? "Sem réplica" : item.dscReplica}
            margin="normal"
            variant="outlined"
          />
          {/* </Grid> */}
          <hr />
        </>
      ))}
  </Grid>
);

export default Content;
