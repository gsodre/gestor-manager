import React from "react";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
// import Fab from "@material-ui/core/Fab";
// import ReplyIcon from "@material-ui/icons/Reply";
// import Tooltip from "@material-ui/core/Tooltip";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";

const Content = ({ states, handleReply, handleChange }) => (
  <Grid container>
    <Table size="small">
      <TableHead>
        <TableRow>
          <TableCell>Loja</TableCell>
          <TableCell>Avaliador</TableCell>
          <TableCell>Função</TableCell>
          <TableCell>Nota</TableCell>
          <TableCell>Contato</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        <TableRow>
          <TableCell>{states.mLoja}</TableCell>
          <TableCell>{states.avaliador}</TableCell>
          <TableCell>{states.funcao}</TableCell>
          <TableCell>{states.nota}</TableCell>
          <TableCell>{states.contato}</TableCell>
        </TableRow>
      </TableBody>
    </Table>
    {/* {states.btReply && (
      <Tooltip title="Cadastrar Resposta" placement="left">
        <Fab
          className="bt-reply"
          size="small"
          color="primary"
          onClick={handleReply}
        >
          <ReplyIcon onClick={handleReply} />
        </Fab>
      </Tooltip>
    )} */}
    <TextField
      disabled={false}
      label="Justificativa"
      multiline
      fullWidth
      rows="4"
      value={states.justificativa}
      margin="normal"
      variant="outlined"
    />
    {/* {states.reply && ( */}
    <TextField
      disabled={!states.btReply}
      disabled={false}
      label="Réplica"
      multiline
      fullWidth
      rows="4"
      value={states.replica}
      margin="normal"
      variant="outlined"
      onChange={handleChange}
    />
    {/* )} */}
  </Grid>
);

export default Content;
