import React from "react";
import Chart from "react-google-charts";
import { withStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";

import { styles } from "~/pages/nps/styles/styles";
import DashCard from "~/pages/nps/components/dash-card";

const DashPieChart = ({ states, classes }) => (
  <DashCard
    pie="card-pie-chart"
    xs={12}
    sm={12}
    md={4}
    color="info"
    ico="fas fa-chart-pie"
  >
    <Chart
      // height="120px"
      chartType="PieChart"
      loader={<CircularProgress className={classes.loader} />}
      data={[
        ["Índice", "Votos"],
        ["Promotores", +`${states.qtdPromotor}`],
        ["Neutros", +`${states.qtdNeutro}`],
        ["Detratores", +`${states.qtdDetrator}`]
      ]}
      options={{
        legend: "none",
        is3D: true,
        slices: {
          0: { color: "#57b05b" },
          1: { color: "#fe9e1a" },
          2: { color: "#e8413d" }
        },
        chartArea: { left: 0, top: 0, width: "100%", height: "100%" }
      }}
    />
  </DashCard>
);

export default withStyles(styles)(DashPieChart);
