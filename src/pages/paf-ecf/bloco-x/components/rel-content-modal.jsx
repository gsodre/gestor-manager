import React from "react";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";

const Content = ({ states, handleReply, handleChange }) => (
  <Grid container justify="center" spacing={1}>
    <Grid md={2} item>
      <TextField
        fullWidth
        disabled
        label="Loja"
        value={states.mLoja}
        margin="normal"
        InputLabelProps={{
          shrink: true
        }}
      />
    </Grid>

    <Grid md={6} item>
      <TextField
        fullWidth
        disabled
        label="Nome Fantasia"
        value={states.mNome}
        margin="normal"
        InputLabelProps={{
          shrink: true
        }}
      />
    </Grid>

    <Grid md={4} item>
      <TextField
        fullWidth
        disabled
        label="Data Início Obrigação"
        value={states.mDataObrigacao}
        margin="normal"
        InputLabelProps={{
          shrink: true
        }}
      />
    </Grid>

    <Table>
      <TableHead>
        <TableRow>
          <TableCell>Tipo</TableCell>
          <TableCell>Nº de Fabricação da ECF</TableCell>
          <TableCell>Situação</TableCell>
          <TableCell>Descrição</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {states.redZ.length > 0 &&
          states.redZ.map(item => (
            <TableRow>
              <TableCell>{item.tipo}</TableCell>
              <TableCell>{item.ecf}</TableCell>
              <TableCell>{item.status}</TableCell>
              <TableCell>{item.desc}</TableCell>
            </TableRow>
          ))}
        {states.estq.length > 0 &&
          states.estq.map(item => (
            <TableRow>
              <TableCell>{item.tipo}</TableCell>
              <TableCell>{item.ecf}</TableCell>
              <TableCell>{item.status}</TableCell>
              <TableCell>{item.desc}</TableCell>
            </TableRow>
          ))}
      </TableBody>
    </Table>
  </Grid>
);

export default Content;
