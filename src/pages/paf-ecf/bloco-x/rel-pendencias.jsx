import React, { Component } from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import Snackbar from "@material-ui/core/Snackbar";

import api from "~/services/api";
import Table from "~/components/Tables/table";
import ModalConfimation from "~/components/modals/modal-confirmation";
import Noty from "~/components/noty-alert";
import Content from "~/pages/paf-ecf/bloco-x/components/rel-content-modal";

class Pesquisa extends Component {
  state = {
    isLoading: true,
    result: false,
    alert: false,
    alertIcon: null,
    alertMessage: null,
    columns: [
      {
        name: "IE",
        options: {
          viewColumns: false,
          print: false,
          download: false,
          display: false
        }
      },
      "Loja",
      "Nome Fantasia",
      "Versão Sistema",

      {
        name: "Data Início Obrigação",
        options: {
          customBodyRender: value => {
            let i = value.split("-");
            return `${i[2]}/${i[1]}/${i[0]}`;
          }
        }
      },
      "Quantidade de Pendências",
      {
        name: "Pendente",
        options: {
          viewColumns: false,
          print: false,
          download: false,
          display: false
        }
      }
    ],
    data: [],
    modal: {
      open: false,
      title: ""
    },
    redZ: [],
    estq: []
  };

  componentDidMount() {
    this.getPendencias();
  }

  getPendencias = async () => {
    try {
      const response = await api.getPendencias();
      console.log(response.data);

      const data = response.data.map(item => [
        item.ie,
        item.loja,
        item.nomeFantasia,
        item.versaoSistema,
        item.dataInicioObrigacao,
        +item.quantidadePendencias,
        +item.quantidadePendencias > 0 ? "Pendente" : ""
      ]);
      data.length > 0
        ? this.setState({ data, result: true, isLoading: false })
        : this.setState({
            isLoading: false,
            result: false,
            alert: true,
            alertIcon: "warning",
            alertMessage: "Sua pesquisa não gerou resultados!"
          });
    } catch (error) {
      let value =
        error.response === undefined
          ? "Serviço Temporariamente Indisponível."
          : error.response.data;
      this.setState({
        error: value,
        isLoading: false
      });
    }
  };

  openDetails = async rowData => {
    if (rowData[5] > 0) {
      try {
        this.setState({ isLoading: true });
        const response = await api.getPendenciasContribuinte(rowData[0]);

        var redZ = [];
        var estq = [];

        if (response.data.ReducoesZ.Ecf.length > 0) {
          response.data.ReducoesZ.Ecf.map(item =>
            redZ.push({
              tipo: "Redução Z",
              ecf: item.NumeroFabricacaoEcf,
              status: item.SituacaoPafEcfDescricao,
              desc: item.Pendencias.Pendencia.Descricao
            })
          );
        } else if (
          response.data.ReducoesZ.Ecf.Pendencias.Pendencia.length > 0
        ) {
          response.data.ReducoesZ.Ecf.Pendencias.Pendencia.map(item =>
            redZ.push({
              tipo: "Redução Z",
              ecf: response.data.ReducoesZ.Ecf.NumeroFabricacaoEcf,
              status: response.data.ReducoesZ.Ecf.SituacaoPafEcfDescricao,
              desc: item.Descricao
            })
          );
        } else {
          redZ.push({
            tipo: "Redução Z",
            ecf: response.data.ReducoesZ.Ecf.NumeroFabricacaoEcf,
            status: response.data.ReducoesZ.Ecf.SituacaoPafEcfDescricao,
            desc: response.data.ReducoesZ.Ecf.Pendencias.Pendencia.Descricao
          });
        }

        if (response.data.Estoques.Estoque.Pendencias.Pendencia > 0) {
          response.data.Estoques.Estoque.Pendencias.Pendencia.map(item =>
            estq.push({
              tipo: "Estoque",
              ecf: "-",
              status: "-",
              desc: item.Descricao
            })
          );
        } else {
          estq.push({
            tipo: "Estoque",
            ecf: "-",
            status: "-",
            desc: response.data.Estoques.Estoque.Pendencias.Pendencia.Descricao
          });
        }

        this.setState({
          isLoading: false,
          modal: {
            open: true,
            title: `Detalhe de Pendências - Loja ${rowData[1]}`
          },
          mIe: rowData[0],
          mLoja: rowData[1],
          mNome: rowData[2],
          mDataObrigacao: rowData[4],
          redZ,
          estq
        });
      } catch (error) {
        let value =
          error.response === undefined
            ? "Serviço Temporariamente Indisponível."
            : error.response.data;

        this.setState({
          isLoading: false,
          result: false,
          alert: true,
          alertIcon: "error",
          alertMessage: value
        });
      }
    } else {
      this.setState({
        alert: true,
        alertIcon: "success",
        alertMessage: "Esta loja não possui pendências."
      });
    }
  };

  handleCancel = () => {
    this.setState(prevState => ({
      modal: {
        open: false,
        title: prevState.modal.title,
        content: prevState.modal.content
      }
    }));
  };

  handleClose = reason => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ alert: false });
  };

  render() {
    const states = this.state;
    return (
      <>
        {states.isLoading && <CircularProgress className="loader" />}
        {states.result && (
          <Table
            title="Lojas com Pendências"
            columns={states.columns}
            data={states.data}
            states={states}
            options={{
              display: false,
              filter: false,
              print: false,
              responsive: "scroll",
              selectableRows: false,
              rowsPerPage: 50,
              rowsPerPageOptions: [50, 100, 200],
              onRowClick: rowData => this.openDetails(rowData)
            }}
          />
        )}

        <ModalConfimation
          maxWidth="md"
          states={states}
          visible={false}
          bts={["Fechar", ""]}
          handleCancel={() => this.handleCancel()}
        >
          <Content states={states} />
        </ModalConfimation>

        <Snackbar
          anchorOrigin={{
            vertical: "top",
            horizontal: "right"
          }}
          open={states.alert}
          autoHideDuration={3000}
          onClose={this.handleClose}
        >
          <Noty
            onClose={this.handleClose}
            variant={states.alertIcon}
            message={states.alertMessage}
          />
        </Snackbar>
      </>
    );
  }
}

export default Pesquisa;
