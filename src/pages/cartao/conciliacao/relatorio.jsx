import React, { Component } from "react";
import * as moment from "moment";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import CircularProgress from "@material-ui/core/CircularProgress";
import Snackbar from "@material-ui/core/Snackbar";

import { apiCartoes } from "~/services/api";
import FilterCard from "~/components/cards/filter-card";
import DatePicker from "~/components/FilterCards/date-picker";
import Select from "~/components/select-simple";
import Table from "~/components/Tables/table";
import Noty from "~/components/noty-alert";

const analitico = [
  { value: true, text: "Analítico" },
  { value: false, text: "Sintético" }
];

class Relatorio extends Component {
  state = {
    isLoading: false,
    result: false,
    cnpj: "",
    dateStart: moment()
      .subtract(1, "months")
      .startOf("month"),
    dateEnd: moment()
      .subtract(1, "months")
      .endOf("month"),
    analitico: false,
    alert: false,
    alertIcon: null,
    alertMessage: null,
    columns: ["CNPJ", "Transações", "Tipo", "Data / Período"],
    data: [],
    modal: {
      open: false,
      title: ""
    }
  };

  componentDidMount() {}

  handleDateChange = opt => date => this.setState({ [opt]: date });

  handleChange = opt => evt => this.setState({ [opt]: evt.target.value });

  getData = async () => {
    this.setState({ isLoading: true });
    const { cnpj, dateStart, dateEnd, analitico } = this.state;
    const dataInicio = moment(dateStart).format("YYYY-MM-DD");
    const dataFim = moment(dateEnd).format("YYYY-MM-DD");

    try {
      const response = await apiCartoes.getRelCards(
        cnpj,
        dataInicio,
        dataFim,
        analitico
      );
      console.log(response.data);
      const data = response.data.map(item => [
        item.cnpj,
        item.quantidade,
        item.tipo === undefined ? "-" : item.tipo,
        analitico === true
          ? moment(item.data).format("DD/MM/YYYY")
          : `${moment(dateStart).format("DD/MM/YYYY")} - ${moment(
              dateEnd
            ).format("DD/MM/YYYY")}`
      ]);
      data.length > 0
        ? this.setState({ data, result: true, isLoading: false })
        : this.setState({
            isLoading: false,
            result: false,
            alert: true,
            alertIcon: "warning",
            alertMessage: "Sua pesquisa não gerou resultados!"
          });

      this.setState({ isLoading: false, result: true });
    } catch (error) {
      let value =
        error.response === undefined
          ? "Serviço Temporariamente Indisponível."
          : error.response.data;

      this.setState({
        isLoading: false,
        result: false,
        alert: true,
        alertIcon: "error",
        alertMessage: value
      });
    }
  };

  handleClose = reason => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ alert: false });
  };

  render() {
    const { classes } = this.props;
    const states = this.state;
    return (
      <>
        {states.isLoading && <CircularProgress className="loader" />}
        <FilterCard
          title="Relatório"
          states={states}
          handleClear={() => this.handleClear()}
          getData={() => this.getData()}
          btAction={{ ico: "fa fa-search", text: "Pesquisar" }}
        >
          <Grid container justify="center" spacing={1}>
            <Grid item xs={12} md={3}>
              <TextField
                disabled={states.isLoading}
                label="CNPJ"
                type="number"
                fullWidth
                margin="dense"
                variant="outlined"
                onChange={evt => this.setState({ cnpj: evt.target.value })}
              />
            </Grid>

            <DatePicker
              label="Período De"
              format="dd/MM/yyyy"
              openTo="year"
              views={["year", "month", "date"]}
              states={states}
              value={states.dateStart}
              classes={classes}
              grid={{ xs: 6, md: 2 }}
              maxDate={new Date()}
              onChange={this.handleDateChange("dateStart")}
            />
            <DatePicker
              label="Até"
              format="dd/MM/yyyy"
              openTo="year"
              views={["year", "month", "date"]}
              states={states}
              value={states.dateEnd}
              classes={classes}
              grid={{ xs: 6, md: 2 }}
              maxDate={new Date()}
              onChange={this.handleDateChange("dateEnd")}
            />

            <Select
              states={states}
              label="Tipo"
              value={states.analitico}
              options={analitico}
              classes={classes}
              grid={{ xs: 6, md: 2 }}
              onChange={this.handleChange("analitico")}
            />
          </Grid>
        </FilterCard>

        {states.result && (
          <Table
            title="Relatório de Transações de Cartões"
            columns={states.columns}
            data={states.data}
            states={states}
            options={{
              display: false,
              filter: false,
              print: false,
              responsive: "scroll",
              selectableRows: false,
              rowsPerPage: 50,
              rowsPerPageOptions: [50, 100, 200],
              downloadOptions: {
                filename: `Transações_de_Cartões_${moment(
                  states.dateStart
                ).format("DD/MM/YYYY")}_ate_${moment(states.dateEnd).format(
                  "DD/MM/YYYY"
                )}.csv`,
                separator: ";"
              }
            }}
          />
        )}

        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          open={states.alert}
          autoHideDuration={3000}
          onClose={this.handleClose}
        >
          <Noty
            onClose={this.handleClose}
            variant={states.alertIcon}
            message={states.alertMessage}
          />
        </Snackbar>
      </>
    );
  }
}

export default Relatorio;
