import { createMuiTheme } from "@material-ui/core/styles";

import cover from "../../../images/cover.jpg";

export const theme = createMuiTheme({
  palette: {
    type: "dark",
    primary: { main: "#FFF" },
    secondary: { main: "#33414e" }
  },
  typography: {
    useNextVariants: true
  }
});

export const styles = () => ({
  img: {
    textAlign: "center"
  },

  loginContainer: {
    backgroundImage: `url(${cover})`,
    height: "-webkit-fill-available",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover"
  },

  loginBox: {
    // margin: '0px auto',
    paddingTop: "200px"
  },

  loginBody: {
    backgroundColor: "rgba(51, 65, 78, 0.6)",
    borderRadius: "8px",
    padding: "20px",
    boxShadow: `0px 1px 5px 0px rgba(0,0,0,0.2),
                0px 2px 2px 0px rgba(0,0,0,0.14),
                0px 3px 1px -2px rgba(0,0,0,0.12)`
  },

  btLogin: {
    marginTop: "20px"
  },

  alert: {
    padding: "20px",
    backgroundColor: "#f44336a3",
    color: "white",
    borderRadius: "6px",
    marginTop: "20px",
    fontStyle: "italic"
  },

  m0: {
    margin: "0px"
  },

  loader: {
    position: "relative",
    left: "50%",
    top: "50%",
    marginLeft: "-20px",
    marginTop: "-26px",
    // background: 'rgba(0, 0, 0, 0.6)',
    zIndex: "9999"
  }
});
