import React, { Component } from "react";
// import PropTypes from 'prop-types';
import { MuiThemeProvider, withStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { withRouter } from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";

import { styles, theme } from "./styles/styles";
import api from "~/services/api";
import { login, isAuthenticated } from "~/services/auth";
import logo from "~/images/logo.png";

class Login extends Component {
  state = {
    user: "",
    pass: "",
    error: "",
    loading: false,
    disabled: false,
    showPass: false
  };

  componentDidMount() {
    this.props.location.state !== undefined
      ? this.setState({ data: this.props.location.state.from.search })
      : this.setState({ data: "" });
    let ecra = document.documentElement.clientHeight;
    document.getElementById("win-login").style.height = `${ecra}px`;
    isAuthenticated() && this.props.history.push("/index");
  }

  handleClickShowPassword = () => {
    this.setState(state => ({ showPass: !state.showPass }));
  };

  handleSignIn = async e => {
    e.preventDefault();
    this.setState({ loading: true, disabled: true });
    const { user, pass, data } = this.state;
    const hash = btoa(`${user}:${pass}`);

    try {
      const response = await api.auth(hash);
      console.log(response);
      const { accessToken, expireIn, usuario } = response.data;

      login(accessToken, expireIn, usuario);

      const hist = this.props;
      hist.history.push(`/index${data}`);
    } catch (err) {
      let value =
        err.response === undefined
          ? "Serviço Temporariamente Indisponível."
          : err.response.data;
      this.setState({
        error: value.mensagem,
        loading: false,
        disabled: false
      });
    }
  };

  render() {
    const { classes } = this.props;
    const states = this.state;
    return (
      <MuiThemeProvider theme={theme}>
        <div id="win-login" className={classes.loginContainer}>
          <Grid
            container
            justify="center"
            alignItems="center"
            className={classes.loginBox}
          >
            <section className={classes.loginBody}>
              <section className={classes.img}>
                <img src={logo} alt="Cigam" />
              </section>
              <form onSubmit={this.handleSignIn}>
                <TextField
                  disabled={states.disabled}
                  margin="dense"
                  type="text"
                  fullWidth
                  label="Usuário"
                  onChange={e => this.setState({ user: e.target.value })}
                />
                <TextField
                  disabled={states.disabled}
                  margin="dense"
                  type={states.showPass ? "text" : "password"}
                  fullWidth
                  label="Senha"
                  onChange={e => this.setState({ pass: e.target.value })}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton onClick={this.handleClickShowPassword}>
                          {states.showPass ? <Visibility /> : <VisibilityOff />}
                        </IconButton>
                      </InputAdornment>
                    )
                  }}
                />
                <Button
                  disabled={states.disabled}
                  className={classes.btLogin}
                  type="submit"
                  fullWidth
                  size="medium"
                  variant="contained"
                  color="secondary"
                >
                  {"Entrar"}
                </Button>
                {states.loading && (
                  <CircularProgress className={classes.loader} />
                )}
                {states.error && (
                  <div className={classes.alert}>
                    <p className={classes.m0}>{states.error}</p>
                  </div>
                )}
              </form>
            </section>
          </Grid>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default withStyles(styles)(withRouter(Login));
