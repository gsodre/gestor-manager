import React, { Component } from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import Snackbar from "@material-ui/core/Snackbar";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";

import Noty from "~/components/noty-alert";
import api from "~/services/api";
import { apiAlertas } from "~/services/api";
import FilterCard from "~/components/cards/filter-card";
import LiveSearch from "~/components/auto-complete";
import DatePicker from "~/components/FilterCards/date-picker";
import Select from "~/components/select-simple";
import Table from "~/components/Tables/table";
import ModalConfimation from "~/components/modals/modal-confirmation";

const tipo = [
  { value: -1, text: "Todos" },
  { value: 0, text: "Cigam" },
  { value: 1, text: "Franquia" }
];

const status = [
  { value: -1, text: "Todos" },
  { value: 0, text: "Não Lida" },
  { value: 1, text: "Lida" }
];

const ativo = [
  { value: "T", text: "Todos" },
  { value: "S", text: "Sim" },
  { value: "N", text: "Não" }
];

const filtro = [
  { value: 0, text: "Data Cadastro" },
  { value: 1, text: "Data Vigência" }
];

const apresent = [
  { value: -1, text: "Todas" },
  { value: 0, text: "Uma Vez" },
  { value: 1, text: "Diário" },
  { value: 2, text: "Semanal" },
  { value: 3, text: "Quinzenal" },
  { value: 4, text: "Mensal" }
];

const columns = [
  {
    name: "cod",
    label: "#",
    options: {
      viewColumns: false,
      display: false
    }
  },
  { name: "desc", label: "Descrição" },
  {
    name: "tipo",
    label: "Tipo",
    options: {
      customBodyRender: value => {
        return value === 0 ? "Cigam" : "Franqueadora";
      }
    }
  },
  { name: "franqueadora", label: "Franqueadora" },
  {
    name: "ativo",
    label: "Ativo",
    options: {
      customBodyRender: value => {
        return value === "S" ? "Sim" : "Não";
      }
    }
  },
  {
    name: "cadastro",
    label: "Cadastro",
    options: {
      display: false
    }
  },
  { name: "vigencia", label: "Vigência" },
  {
    name: "apresentacao",
    label: "Apresentação",
    options: {
      customBodyRender: value => {
        return value === 0
          ? "Uma Vez"
          : value === 1
          ? "Diário"
          : value === 2
          ? "Semanal"
          : value === 3
          ? "Quinzenal"
          : "Mensal";
      }
    }
  },
  {
    name: "dimensoes",
    label: "Dimensões",
    options: {
      display: false
    }
  },
  {
    name: "url",
    label: "URL",
    options: {
      display: false
    }
  },
  { name: "qtdAlertas", label: "Qtd. Alertas" },
  { name: "qtdVisualizados", label: "Qtd. Visualizados" }
];

const detailsColumns = [
  { name: "loja", label: "Loja" },
  {
    name: "flgStatus",
    label: "Status",
    options: {
      customBodyRender: value => {
        return value === 0 ? "Não Lida" : "Lida";
      }
    }
  },
  { name: "leitura", label: "Envio PDV" },
  { name: "apresentacao", label: "Última Apresentação" },
  { name: "qtdApresentada", label: "Qtd. Apresentada" }
];

class RelatorioAlertas extends Component {
  state = {
    isLoading: true,
    result: false,
    alert: false,
    alertIcon: null,
    alertMessage: null,
    franquia: [],
    idFranquia: null,
    mostrarInativas: false,
    mostrarRetaguardas: false,
    mostrarDesinstaladas: false,
    loja: [],
    idLoja: null,
    tipo: -1,
    status: -1,
    ativo: "T",
    filtro: 0,
    apresent: -1,
    dateStart: null,
    dateEnd: null,
    columns,
    data: [],
    detailsColumns,
    detailsData: [],
    vigencia: "",
    modal: {
      open: false,
      title: ""
    }
  };

  async componentDidMount() {
    await this.getFranquias();
  }

  handleClear = () =>
    this.setState({
      result: false
    });

  getFranquias = async () => {
    try {
      const response = await api.getFranquias();
      const franquia = response.data.map((item, index) => ({
        value: index === 0 ? 0 : item.idfranquia,
        label: index === 0 ? "CIGAM" : item.nome
      }));
      this.setState({ franquia, isLoading: false });
    } catch (error) {
      let value =
        error.response === undefined
          ? "Serviço Temporariamente Indisponível."
          : error.response.data;
      this.setState({
        isLoading: false,
        alert: true,
        alertIcon: "error",
        alertMessage: value
      });
    }
  };

  getLojas = async idFranquia => {
    const {
      mostrarInativas,
      mostrarRetaguardas,
      mostrarDesinstaladas
    } = this.state;
    this.setState({ isLoading: true });

    try {
      const response = await api.getLojas(
        idFranquia,
        mostrarInativas,
        mostrarRetaguardas,
        mostrarDesinstaladas
      );
      const loja = response.data.map(item => ({
        value: item.identificacaoLoja,
        label: `${item.identificacaoLoja} - ${item.nome}`
      }));
      console.log(response);
      this.setState({ loja, isLoading: false });
    } catch (error) {
      let value =
        error.response === undefined
          ? "Serviço Temporariamente Indisponível."
          : error.response.data;
      this.setState({
        isLoading: false,
        alert: true,
        alertIcon: "error",
        alertMessage: value
      });
    }
  };

  handleChange = opt => event => {
    let value = null;
    event === null
      ? (value = null)
      : (value =
          opt === "idFranquia" || opt === "idLoja"
            ? event.value
            : event.target.value);
    this.setState({
      [opt]: value
    });
    value !== null && opt === "idFranquia"
      ? this.getLojas(value)
      : this.setState({ isLoading: false });
  };

  handleDateChange = opt => date => this.setState({ [opt]: date });

  getData = async () => {
    const {
      idFranquia,
      idLoja,
      desc,
      tipo,
      status,
      ativo,
      apresent,
      filtro,
      dateStart,
      dateEnd
    } = this.state;

    let codFranqueadora = idFranquia;
    let loja = idLoja;
    let dscAlertaSistema = desc;
    let flgTipoAlerta = tipo;
    let flgStatus = status;
    let flgAtivo = ativo;
    let tipoData = filtro;
    let datInicio = dateStart;
    let datFim = dateEnd;
    let flgTipoApresentacao = apresent;

    console.log(
      codFranqueadora,
      loja,
      dscAlertaSistema,
      flgTipoAlerta,
      flgStatus,
      flgAtivo,
      tipoData,
      datInicio,
      datFim,
      flgTipoApresentacao
    );

    this.setState({ isLoading: true });

    try {
      const response = await apiAlertas.getAlertas(
        codFranqueadora,
        loja,
        dscAlertaSistema,
        flgTipoAlerta,
        flgStatus,
        flgAtivo,
        tipoData,
        datInicio,
        datFim,
        flgTipoApresentacao
      );

      console.log(response.data);

      const data = response.data.map(item => [
        item.codAlertaSistema,
        item.dscAlertaSistema,
        item.flgTipoAlerta,
        item.nomeFranqueadora,
        item.flgAtivo,
        `${item.datCadastro} - ${item.horCadastro}`,
        `${item.datInicio} - ${item.datFim}`,
        item.flgTipoApresentacao,
        `${item.defLargura}px x ${item.defAltura}px`,
        item.dscUrl,
        item.qtdAlertas,
        item.qtdVisualizados
      ]);
      this.setState({ isLoading: false, result: true, data });
    } catch (error) {
      let value =
        error.response === undefined
          ? "Serviço Temporariamente Indisponível."
          : error.response.data;
      this.setState({
        isLoading: false,
        alert: true,
        alertIcon: "error",
        alertMessage: value
      });
    }
  };

  openDatails = async rowData => {
    console.log(rowData);

    if (rowData[10] > 0) {
      try {
        this.setState({ isLoading: true });
        const response = await apiAlertas.getAlertasLoja(rowData[0]);

        if (response.status === 200) {
          console.log(response);

          const detailsData = response.data.map(item => [
            item.loja,
            item.flgStatus,
            item.datLida !== null ? `${item.datLida} - ${item.horLida}` : "",
            item.datApresentada !== null
              ? `${item.datApresentada} as ${item.horApresentada}`
              : "",
            item.qtdApresentada
          ]);

          this.setState({
            isLoading: false,
            detailsData,
            vigencia: rowData[6],
            modal: {
              open: true,
              title: rowData[1]
            }
          });

          console.log(this.state);
        } else {
          throw new Error("");
        }
      } catch (error) {
        let value =
          error.response === undefined
            ? "Serviço Temporariamente Indisponível."
            : error.response.data;

        this.setState({
          isLoading: false,
          result: false,
          alert: true,
          alertIcon: "error",
          alertMessage: value
        });
      }
    } else {
      this.setState({
        alert: true,
        alertIcon: "warning",
        alertMessage: "Este alerta não possui lojas."
      });
    }
  };

  handleCancel = () => {
    this.setState(prevState => ({
      modal: {
        open: false,
        title: prevState.modal.title,
        content: prevState.modal.content
      }
    }));
  };

  handleClose = reason => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ alert: false });
  };

  render() {
    const { classes } = this.props;
    const states = this.state;
    return (
      <>
        {states.isLoading && <CircularProgress className="loader" />}

        <FilterCard
          title="Relatório Sistema de Alertas"
          states={states}
          handleClear={() => this.handleClear()}
          getData={() => this.getData()}
          btClear={true}
          btAction={{ ico: "fa fa-search", text: "Pesquisar" }}
        >
          <Grid container justify="center" spacing={1}>
            <LiveSearch
              isDisabled={states.isLoading}
              placeholder="Todas"
              label="Franquia"
              states={states}
              grid={{ xs: 6, md: 3 }}
              options={states.franquia}
              onChange={this.handleChange("idFranquia")}
            />

            <LiveSearch
              isDisabled={states.isLoading || states.idFranquia === null}
              placeholder="Todas"
              label="Loja"
              states={states}
              grid={{ xs: 6, md: 3 }}
              options={states.loja}
              onChange={this.handleChange("idLoja")}
            />

            <Grid xs={6} md={3} item>
              <TextField
                disabled={states.isLoading}
                fullWidth
                label="Descrição"
                variant="outlined"
                // value={states.mNome}
                margin="dense"
                onChange={e => this.setState({ desc: e.target.value })}
              />
            </Grid>

            <Select
              label="Tipo"
              states={states}
              value={states.tipo}
              options={tipo}
              classes={classes}
              grid={{ xs: 6, md: 2 }}
              onChange={this.handleChange("tipo")}
            />

            <Select
              label="Status"
              states={states}
              value={states.status}
              options={status}
              classes={classes}
              grid={{ xs: 6, md: 2 }}
              onChange={this.handleChange("status")}
            />

            <Select
              label="Ativo"
              states={states}
              value={states.ativo}
              options={ativo}
              classes={classes}
              grid={{ xs: 6, md: 2 }}
              onChange={this.handleChange("ativo")}
            />

            <Select
              label="Apresentação"
              states={states}
              value={states.apresent}
              options={apresent}
              classes={classes}
              grid={{ xs: 6, md: 2 }}
              onChange={this.handleChange("apresent")}
            />

            <Select
              label="Filtrar por"
              states={states}
              value={states.filtro}
              options={filtro}
              classes={classes}
              grid={{ xs: 6, md: 2 }}
              onChange={this.handleChange("filtro")}
            />

            <DatePicker
              label="Período De"
              format="dd/MM/yyyy"
              openTo="year"
              views={["year", "month", "date"]}
              states={states}
              value={states.dateStart}
              classes={classes}
              grid={{ xs: 6, md: 2 }}
              maxDate={new Date()}
              onChange={this.handleDateChange("dateStart")}
            />

            <DatePicker
              label="Até"
              format="dd/MM/yyyy"
              openTo="year"
              views={["year", "month", "date"]}
              states={states}
              value={states.dateEnd}
              classes={classes}
              grid={{ xs: 6, md: 2 }}
              maxDate={new Date()}
              onChange={this.handleDateChange("dateEnd")}
            />
          </Grid>
        </FilterCard>

        {states.result && (
          <Table
            title="Alertas"
            columns={states.columns}
            data={states.data}
            states={states}
            options={{
              display: false,
              filter: false,
              print: false,
              responsive: "stacked",
              selectableRows: "none",
              rowsPerPage: 20,
              rowsPerPageOptions: [20, 50, 100],
              onRowClick: rowData => this.openDatails(rowData)
              // downloadOptions: {
              //   filename: `Alerta_${states.modal.title}.csv`
              // }
            }}
          />
        )}

        <ModalConfimation
          maxWidth="xl"
          states={states}
          visible={false}
          bts={["Fechar", ""]}
          handleCancel={() => this.handleCancel()}
        >
          <Table
            title=""
            columns={states.detailsColumns}
            data={states.detailsData}
            states={states}
            options={{
              display: false,
              filter: false,
              print: false,
              responsive: "stacked",
              selectableRows: "none",
              rowsPerPage: 20,
              rowsPerPageOptions: [20, 50, 100],
              downloadOptions: {
                filename: `Rel_${states.modal.title.replace(" ", "_")}_${
                  states.vigencia
                }.csv`
              }
            }}
          />
        </ModalConfimation>

        <Snackbar
          anchorOrigin={{
            vertical: "top",
            horizontal: "right"
          }}
          open={states.alert}
          autoHideDuration={3000}
          onClose={this.handleClose}
        >
          <Noty
            onClose={this.handleClose}
            variant={states.alertIcon}
            message={states.alertMessage}
          />
        </Snackbar>
      </>
    );
  }
}

export default RelatorioAlertas;
