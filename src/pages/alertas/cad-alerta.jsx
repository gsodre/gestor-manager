import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import CircularProgress from "@material-ui/core/CircularProgress";
import Grow from "@material-ui/core/Grow";
import Snackbar from "@material-ui/core/Snackbar";
import { isBefore, format } from "date-fns";
import locale from "date-fns/locale/pt-BR";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";

import Noty from "~/components/noty-alert";
import "~/pages/alertas/styles/styles.css";
import api from "~/services/api";
import FilterCard from "~/components/cards/filter-card";
import DateTimePicker from "~/components/date-time-picker";
import Select from "~/components/select-simple";
import Autocomplete from "~/components/autocomplete-check-tags";
import LiveSearch from "~/components/auto-complete";
import Table from "~/components/Tables/table";
import Btn from "~/components/buttons/bt-icon-right";
import ModalYesNo from "~/components/modals/modal-yn";
import { apiAlertas } from "~/services/api";

const flgTipoApresentacao = [
  { value: 0, text: "Uma vez" },
  { value: 1, text: "Diário" },
  { value: 2, text: "Semanal" },
  { value: 3, text: "Quinzenal" },
  { value: 4, text: "Mensal" }
];

// const flgFecharAlerta = [{ value: 0, text: "Não" }, { value: 1, text: "Sim" }];

const columns = [
  {
    name: "cod",
    label: "#",
    options: {
      viewColumns: false,
      display: false
    }
  },
  {
    name: "id",
    label: "ID Franquia",
    options: {
      viewColumns: false,
      display: false
    }
  },
  { name: "franquia", label: "Franquia" },
  { name: "loja", label: "Loja" },
  { name: "excecao", label: "Exceção" },
  {
    name: "todasLojas",
    label: "Lojas",
    options: {
      viewColumns: false,
      display: false
    }
  },
  {
    name: "todasExcecoes",
    label: "Exceções",
    options: {
      viewColumns: false,
      display: false
    }
  }
];

class CadAlerta extends Component {
  state = {
    dscAlertaSistema: "",
    datInicio: new Date(),
    datFim: new Date(),
    horInicio: format(new Date(), "kk:mm", { locale }),
    horFim: format(new Date(), "kk:mm", { locale }),
    defAltura: "",
    defLargura: "",
    dscUrl: "",
    flgTipoApresentacao: 0,
    flgFecharAlerta: 0,
    clean: true,
    isLoading: false,
    alert: false,
    alertIcon: null,
    alertMessage: null,
    franquia: [],
    mostrarInativas: false,
    mostrarRetaguardas: false,
    mostrarDesinstaladas: false,
    idFranquia: null,
    columns,
    data: [],
    lojas: [],
    excecao: false,
    addLojas: false, //apresenta seletor de franquias e lojas
    logout: {
      open: false,
      title: "Atenção!",
      msg:
        "Esta Franquia já possui uma configuração, deseja alterar este registro?"
    },
    edit: false
  };

  async componentDidMount() {
    await this.getFranquias();
  }

  getFranquias = async () => {
    try {
      const response = await api.getFranquias();
      const franquia = response.data.map((item, index) => ({
        value: index === 0 ? 0 : item.idfranquia,
        label: index === 0 ? "CIGAM" : item.nome
      }));
      this.setState({ franquia, isLoading: false });
    } catch (error) {
      let value =
        error.response === undefined
          ? "Serviço Temporariamente Indisponível."
          : error.response.data;
      this.setState({
        isLoading: false,
        alert: true,
        alertIcon: "error",
        alertMessage: value
      });
    }
  };

  getLojas = async idFranquia => {
    const {
      mostrarInativas,
      mostrarRetaguardas,
      mostrarDesinstaladas
    } = this.state;
    this.setState({ isLoading: true, clean: false });
    try {
      const response = await api.getLojas(
        idFranquia,
        mostrarInativas,
        mostrarRetaguardas,
        mostrarDesinstaladas
      );
      const loja = response.data.map(item => ({
        value: item.identificacaoLoja,
        label: `${item.identificacaoLoja} - ${item.nome}`
      }));
      console.log(response);
      this.setState({ loja, isLoading: false, clean: true, lojas: [] });
    } catch (error) {
      let value =
        error.response === undefined
          ? "Serviço Temporariamente Indisponível."
          : error.response.data;
      this.setState({
        isLoading: false,
        alert: true,
        alertIcon: "error",
        alertMessage: value
      });
    }
  };

  handleChange = opt => event => {
    let value = null;
    event === null
      ? (value = null)
      : (value = opt === "idFranquia" ? event.value : event.target.value);
    this.setState({
      [opt]: value
    });
    if (value !== null && opt === "idFranquia") {
      this.getLojas(value);
      this.setState({ nomeLoja: event.label });
    } else {
      this.setState({ isLoading: false });
    }
  };

  handleLojas = opt => this.setState({ lojas: opt });

  validaTop = () => {
    const {
      dscAlertaSistema,
      datInicio,
      datFim,
      defAltura,
      defLargura,
      dscUrl
    } = this.state;

    if (dscAlertaSistema === "") throw "Insira uma descrição!";

    if (isBefore(datFim, datInicio)) throw "Período incompatível!";

    if (defAltura === "") throw "Insira uma Altura!";

    if (defLargura === "") throw "Insira uma Largura!";

    if (dscUrl === "") throw "Insira a URL da imagem!";
  };

  saveData = async () => {
    const {
      dscAlertaSistema,
      datInicio,
      datFim,
      horInicio,
      horFim,
      defAltura,
      defLargura,
      dscUrl,
      flgTipoApresentacao,
      flgFecharAlerta,
      numApresentacao,
      data
    } = this.state;
    let lojas = [],
      lojasExcecao = [];

    try {
      this.setState({ isLoading: true });
      if (data.length === 0)
        throw "Selecione as lojas que receberão este alerta!";

      data.map(item =>
        item.todasLojas !== undefined
          ? (lojas = lojas.concat(item.todasLojas))
          : null
      );

      data.map(item =>
        item.todasExcecoes !== undefined
          ? (lojasExcecao = lojasExcecao.concat(item.todasExcecoes))
          : null
      );

      const model = {
        dscAlertaSistema,
        datInicio,
        datFim,
        horInicio,
        horFim,
        defAltura,
        defLargura,
        dscUrl,
        flgTipoApresentacao,
        flgFecharAlerta,
        numApresentacao: numApresentacao === undefined ? 0 : numApresentacao,
        lojas,
        lojasExcecao
      };

      if (model.lojas.length > 0) {
        const response = await apiAlertas.postAlerta(model);
        if (response.status === 200) {
          this.setState({
            isLoading: false,
            alert: true,
            alertIcon: "success",
            alertMessage: "Alerta Cadastrado com Sucesso!"
          });
          setTimeout(() => this.props.history.push("/alertas/relatorio"), 3000);
        } else {
          throw "Algo de inesperado aconteceu, tente novamente!";
        }
      } else {
        throw "Selecione ao menos uma loja para receber este alerta!";
      }
    } catch (alertMessage) {
      this.setState({
        isLoading: false,
        alert: true,
        alertIcon: "warning",
        alertMessage
      });
    }
  };

  nextStep = async () => {
    console.log(this.state);
    try {
      await this.validaTop();
      this.setState({ addLojas: true });
    } catch (alertMessage) {
      this.setState({
        isLoading: false,
        alert: true,
        alertIcon: "warning",
        alertMessage
      });
    }
  };

  addGrid = () => {
    const { nomeLoja, idFranquia, lojas, data, excecao } = this.state;
    let reg = data.filter(item => item.franquia === nomeLoja).length;
    let loja, todasLojas, excessaoLojas;

    if (reg === 0) {
      if (lojas.length > 0) {
        loja = lojas.map(item => item.value);
        todasLojas = loja;
      } else {
        loja = "Todas";
        todasLojas = this.state.loja.map(item => item.value);
      }

      let linha = {
        cod: 0,
        id: idFranquia,
        franquia: nomeLoja,
        loja: !excecao ? loja.toString() : "-",
        excecao: excecao ? loja.toString() : "-"
      };

      if (excecao) {
        linha.todasExcecoes = todasLojas;
      } else {
        linha.todasLojas = todasLojas;
      }

      console.log(excessaoLojas);
      this.setState(prevState => ({
        data: [linha, ...prevState.data],
        excecao: false
      }));
    } else {
      this.setState(prevState => ({
        logout: {
          open: true,
          title: prevState.logout.title,
          msg: prevState.logout.msg
        }
      }));
    }
  };

  setEdit = async rowData => {
    await this.getLojas(rowData[1]);

    this.setState({
      idFranquia: rowData[1],
      edit: true
    });
  };

  editGrid = async () => {
    const { nomeLoja, lojas, data, excecao, idFranquia } = this.state;
    let loja, todasLojas;

    if (lojas.length > 0) {
      loja = lojas.map(item => item.value);
      todasLojas = loja;
    } else {
      loja = "Todas";
      todasLojas = this.state.loja.map(item => item.value);
    }

    let linha = {
      od: 0,
      id: idFranquia,
      franquia: nomeLoja,
      loja: !excecao ? loja.toString() : "-",
      excecao: excecao ? loja.toString() : "-",
      todasLojas: excecao ? "" : todasLojas.toString(),
      todasExcecoes: excecao ? todasLojas.toString() : ""
    };

    let newData = [...data];

    newData.map((item, index) =>
      item.franquia === nomeLoja ? newData.splice(index, 1, linha) : null
    );

    this.setState({ data: newData, edit: false, excecao: false });
  };

  removeGrid = async row => {
    const { data } = this.state;
    let newData = [];

    row.data.map(item => (data[item.index].cod = 1));
    data.map(item => (item.cod !== 1 ? newData.push(item) : null));
    this.setState({ data: newData });
  };

  handleYes = () => {
    this.setState(prevState => ({
      edit: true,
      logout: {
        open: false,
        title: prevState.logout.title,
        msg: prevState.logout.msg
      }
    }));
  };

  handleNo = () => {
    this.setState(prevState => ({
      logout: {
        open: false,
        title: prevState.logout.title,
        msg: prevState.logout.msg
      }
    }));
  };

  handleClose = reason => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ alert: false });
  };

  handleDateChange = (data, hora) => date => {
    this.setState({ [data]: date, [hora]: format(date, "kk:mm", { locale }) });
  };

  render() {
    const states = this.state;
    return (
      <>
        {states.isLoading && <CircularProgress className="loader" />}

        <FilterCard
          title="Cadastro de Alertas"
          states={states}
          handleClear={() => this.handleClear()}
          btClear={false}
          getData={
            states.addLojas ? () => this.saveData() : () => this.nextStep()
          }
          btAction={
            states.addLojas
              ? { ico: "fas fa-save", text: "Salvar" }
              : { ico: "fas fa-angle-double-right", text: "Próximo" }
          }
        >
          <Grid container justify="center" spacing={1}>
            <Grid xs={6} md={4} item>
              <TextField
                disabled={states.isLoading}
                fullWidth
                label="Descrição"
                variant="outlined"
                value={states.dscAlertaSistema}
                margin="dense"
                onChange={e =>
                  this.setState({ dscAlertaSistema: e.target.value })
                }
              />
            </Grid>

            <DateTimePicker
              label="Período De"
              format="dd/MM/yyyy - HH:mm"
              ampm={false}
              states={states}
              value={states.datInicio}
              grid={{ xs: 6, md: 2 }}
              minDate={new Date()}
              onChange={this.handleDateChange("datInicio", "horInicio")}
            />
            <DateTimePicker
              label="Até"
              format="dd/MM/yyyy - HH:mm"
              ampm={false}
              states={states}
              value={states.datFim}
              grid={{ xs: 6, md: 2 }}
              minDate={new Date()}
              onChange={this.handleDateChange("datFim", "horFim")}
            />

            <Select
              label="Apresentação"
              states={states}
              value={states.flgTipoApresentacao}
              options={flgTipoApresentacao}
              grid={{ xs: 6, md: 2 }}
              onChange={e =>
                this.setState({ flgTipoApresentacao: e.target.value })
              }
            />

            <Grid xs={6} md={2} item>
              <TextField
                disabled={states.isLoading}
                fullWidth
                type="number"
                label="Qtd. Apresentação"
                variant="outlined"
                value={states.numApresentacao}
                margin="dense"
                onChange={e =>
                  this.setState({ numApresentacao: e.target.value })
                }
              />
            </Grid>
          </Grid>

          <Grid container justify="center" alignItems="flex-end" spacing={1}>
            <Grid xs={6} md={2} item>
              <TextField
                disabled={states.isLoading}
                fullWidth
                placeholder="em Pixels"
                type="number"
                label="Largura"
                variant="outlined"
                value={states.defLargura}
                margin="dense"
                onChange={e => this.setState({ defLargura: e.target.value })}
              />
            </Grid>

            <Grid xs={6} md={2} item>
              <TextField
                disabled={states.isLoading}
                fullWidth
                placeholder="em Pixels"
                type="number"
                label="Altura"
                variant="outlined"
                value={states.defAltura}
                margin="dense"
                onChange={e => this.setState({ defAltura: e.target.value })}
              />
            </Grid>

            <Grid xs={6} md={2} item>
              <TextField
                disabled={states.isLoading}
                fullWidth
                label="URL"
                variant="outlined"
                value={states.dscUrl}
                margin="dense"
                onChange={e => this.setState({ dscUrl: e.target.value })}
              />
            </Grid>

            <div className="checkbox">
              <FormControlLabel
                label="Não exibir novamente ao fechar."
                control={<Checkbox checked={states.flgFecharAlerta} />}
                onChange={e =>
                  this.setState({ flgFecharAlerta: +e.target.checked })
                }
              />
            </div>
          </Grid>

          {states.addLojas && (
            <Grow in={states.addLojas}>
              <section>
                <hr className="separador" />

                <Grid container justify="center" spacing={1}>
                  <LiveSearch
                    isDisabled={states.isLoading || states.edit}
                    placeholder="Selecione"
                    label="Franquia"
                    states={states}
                    grid={{ xs: 6, md: 3 }}
                    options={states.franquia}
                    value={states.franquia.filter(
                      item => item.value === states.idFranquia
                    )}
                    onChange={this.handleChange("idFranquia")}
                  />
                  <Btn
                    grid={{ xs: 6, md: 2, lg: 1 }}
                    label={states.excecao ? "Lojas" : "Exceçôes"}
                    ico="fas fa-sync-alt"
                    icoSide="left"
                    onClick={() =>
                      this.setState(state => ({ excecao: !state.excecao }))
                    }
                  />
                </Grid>

                <Grid container justify="center" spacing={1}>
                  {states.clean && (
                    <Autocomplete
                      disabled={states.isLoading || states.idFranquia === null}
                      grid={{ xs: 6, md: 10 }}
                      label={states.excecao ? "NÂO enviar para" : "Enviar para"}
                      options={states.loja}
                      // defaultValue={states.teste}
                      placeholder="Todas"
                      onChange={(e, v) => this.handleLojas(v)}
                    />
                  )}
                  {!states.clean && (
                    <Autocomplete
                      disabled={true}
                      grid={{ xs: 6, md: 10 }}
                      label={states.excecao ? "NÂO enviar para" : "Enviar para"}
                      defaultValue={[]}
                      placeholder="Todas"
                    />
                  )}

                  <Btn
                    disabled={states.isLoading || states.idFranquia === null}
                    grid={{ xs: 6, md: 2, lg: 1 }}
                    label={states.edit ? "editar" : "adicionar"}
                    ico="fas fa-sign-in-alt rotate-90"
                    icoSide="right"
                    onClick={
                      states.edit ? () => this.editGrid() : () => this.addGrid()
                    }
                  />
                </Grid>
              </section>
            </Grow>
          )}

          {states.data.length > 0 && (
            <Table
              className="tbl-alertas"
              title="Alertas"
              columns={states.columns}
              data={states.data}
              states={states}
              options={{
                display: false,
                filter: false,
                print: false,
                download: false,
                viewColumns: false,
                responsive: "stacked",
                // selectableRows: "none",
                rowsPerPage: 10,
                onRowsDelete: rowsDeleted => this.removeGrid(rowsDeleted),
                rowsPerPageOptions: [10, 20, 50, 100],
                onRowClick: rowData => this.setEdit(rowData)
              }}
            />
          )}
        </FilterCard>

        <ModalYesNo
          states={states.logout}
          bts={["Não", "Sim"]}
          handleNo={() => this.handleNo()}
          handleYes={e => this.handleYes(e)}
        />

        <Snackbar
          anchorOrigin={{
            vertical: "top",
            horizontal: "right"
          }}
          open={states.alert}
          autoHideDuration={3000}
          onClose={this.handleClose}
        >
          <Noty
            onClose={this.handleClose}
            variant={states.alertIcon}
            message={states.alertMessage}
          />
        </Snackbar>
      </>
    );
  }
}

export default CadAlerta;
