import React, { Component } from "react";
import Icon from "@material-ui/core/Icon";

import GridContainer from "~/components/Grids/grid-container";
import GridItem from "~/components/Grids/grid-item";
import Card from "~/components/cards/card";
import CardHeader from "~/components/cards/card-header";
import CardIcon from "~/components/cards/card-icon";
import CardFooter from "~/components/cards/card-footer";

class Home extends Component {
  render() {
    return (
      <GridContainer>
        <GridItem xs={12} sm={6} md={3}>
          <Card>
            <CardHeader stats icon>
              <CardIcon color="danger">
                <Icon>content_copy</Icon>
              </CardIcon>
              <p>Used Space</p>
              <h3>
                49/50 <small>GB</small>
              </h3>
            </CardHeader>
            {/* <CardBody>ttesta!</CardBody> */}
            <CardFooter stats>
              <div>Indíce</div>
            </CardFooter>
          </Card>
        </GridItem>
      </GridContainer>
    );
  }
}

export default Home;
