import React from "react";
import { HashRouter, Route, Switch, Redirect } from "react-router-dom";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import ptLocale from "date-fns/locale/pt-BR";

import { isAuthenticated } from "~/services/auth";
import Login from "~/pages/Login/Login";
// import Home from "~/pages/Home/Home";
import Template from "~/pages/Template/template";
import Config from "~/pages/nps/config";
import Report from "~/pages/nps/report";
import RelPersistido from "~/pages/nps/rel-persistido";
import Dashboard from "~/pages/nps/dashboard";
import Setup from "~/pages/portalweb/api-installer/setup";
import Pesquisa from "~/pages/paf-ecf/bloco-x/rel-pendencias";
import CardRport from "~/pages/cartao/conciliacao/relatorio";
import RelatorioAlertas from "~/pages/alertas";
import CrtlRetaguarda from "~/pages/portalweb/ctrl-retaguarda/index";
import CadAlerta from "~/pages/alertas/cad-alerta";

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      isAuthenticated() ? (
        <Component {...props} />
      ) : (
        <Redirect to={{ pathname: "/", state: { from: props.location } }} />
      )
    }
  />
);

const Routes = () => (
  <MuiPickersUtilsProvider utils={DateFnsUtils} locale={ptLocale}>
    <HashRouter>
      <Switch>
        <Route exact path="/" component={Login} />
        <Template>
          <PrivateRoute exact path="/index" component={Dashboard} />
          <PrivateRoute exact path="/nps/dashboard" component={Dashboard} />
          <PrivateRoute exact path="/nps/report" component={Report} />
          <PrivateRoute
            exact
            path="/nps/rel-persistidos"
            component={RelPersistido}
          />
          <PrivateRoute exact path="/nps/config" component={Config} />
          <PrivateRoute exact path="/web/setup" component={Setup} />
          <PrivateRoute exact path="/web/monitor" component={CrtlRetaguarda} />
          <PrivateRoute exact path="/paf/blocox" component={Pesquisa} />
          <PrivateRoute
            exact
            path="/alertas/relatorio"
            component={RelatorioAlertas}
          />
          <PrivateRoute exact path="/alertas/cadastro" component={CadAlerta} />
          <PrivateRoute
            exact
            path="/cartao/conciliacao"
            component={CardRport}
          />
        </Template>
        <Route path="*" component={() => <h1>Page not found</h1>} />
      </Switch>
    </HashRouter>
  </MuiPickersUtilsProvider>
);

export default Routes;
